object Libs {
    const val androidGradlePlugin = "com.android.tools.build:gradle:7.0.0"

    object Kotlin {
        private const val version = "1.5.21"
        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$version"
        const val gradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$version"

        object Coroutines {
            private const val version = "1.5.1"
            const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
            const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$version"
            const val test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:$version"
        }
    }

    object AndroidX {
        const val coreKtx = "androidx.core:core-ktx:1.6.0-rc01"
        const val material = "com.google.android.material:material:1.4.0"

        object Activity {
            const val activityCompose = "androidx.activity:activity-compose:1.3.0"
        }

        object Lifecycle {
            const val viewModelCompose =
                "androidx.lifecycle:lifecycle-viewmodel-compose:1.0.0-alpha07"
        }

        object Material {
        }

        object Test {
            private const val version = "1.3.0"
            private const val espressoVersion = "3.3.0"
            const val runner = "androidx.test:runner:$version"
            const val core = "androidx.test:core:$version"
            const val rules = "androidx.test:rules:$version"
            const val espressoCore = "androidx.test.espresso:espresso-core:$espressoVersion"
        }
    }

    object Navigation {
        const val navigationCompose = "androidx.navigation:navigation-compose:2.4.0-alpha04"
    }

    object Coil {
        const val coilCompose = "io.coil-kt:coil-compose:1.3.2"
    }

    object Compose {
        const val version = "1.0.1"

        const val runtime = "androidx.compose.runtime:runtime:$version"
        const val foundation = "androidx.compose.foundation:foundation:$version"
        const val layout = "androidx.compose.foundation:foundation-layout:$version"
        const val ui = "androidx.compose.ui:ui:$version"
        const val uiUtil = "androidx.compose.ui:ui-util:$version"
        const val animation = "androidx.compose.animation:animation:$version"
        const val material = "androidx.compose.material:material:$version"
        const val iconsExtended = "androidx.compose.material:material-icons-extended:$version"
        const val tooling = "androidx.compose.ui:ui-tooling:$version"
        //debugImplementation
        const val toolingPreview = "androidx.compose.ui:ui-tooling-preview:$version"

        object Accompanist {
            private const val version = "0.16.0"
            const val insets = "com.google.accompanist:accompanist-insets:$version"
            const val pager = "com.google.accompanist:accompanist-pager:$version"
            const val systemUiController =
                "com.google.accompanist:accompanist-systemuicontroller:$version"
        }

        object Test {
            //androidTestImplementation
            const val uiTest = "androidx.compose.ui:ui-test-junit4:$version"
            // Needed for createComposeRule, but not createAndroidComposeRule
            //debugImplementation
            const val testManifest = "androidx.compose.ui:ui-test-manifest:$version"
        }
    }

    object ComposeAdditionalLibs {
        const val composeCollapsingToolbar = "me.onebone:toolbar-compose:2.1.2"
    }

    object AdditionalUiLibs {
        const val mpChart = "com.github.PhilJay:MPAndroidChart:v3.1.0"
    }

    object ConstraintLayout {
        const val constraintLayoutCompose =
            "androidx.constraintlayout:constraintlayout-compose:1.0.0-beta02"
    }

    object Retrofit {
        private const val version = "2.9.0"
        const val retrofit = "com.squareup.retrofit2:retrofit:$version"
        const val retrofitGson = "com.squareup.retrofit2:converter-gson:$version"
        const val retrofitRx = "com.squareup.retrofit2:adapter-rxjava2:$version"
    }

    object Serialization {
        const val gson = "com.google.code.gson:gson:2.8.6"
    }

    object Rx {
        private const val rxVersion = "2.2.21"
        private const val rxAndroidVersion = "2.1.1"
        private const val rxKotlinVersion = "2.4.0"

        const val rxJava = "io.reactivex.rxjava2:rxjava:$rxVersion"
        const val rxKotlin = "io.reactivex.rxjava2:rxkotlin:$rxKotlinVersion"
        const val rxAndroid = "io.reactivex.rxjava2:rxandroid:$rxAndroidVersion"
    }

    object Test {

        object JUnit {
            private const val version = "1.1.2"
            const val junit = "androidx.test.ext:junit-ktx:$version"
        }
    }
}
