package com.sample.compose.dietplan.presentation.ui

import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.junit4.createComposeRule
import com.sample.compose.MainActivity
import com.sample.compose.theme.SampleAppTheme
import org.junit.Rule
import org.junit.Test

//TODO : Learning In Progress
class DiyDietPlanPageTest {

    @get:Rule
    val androidComposeTestRule = createAndroidComposeRule<MainActivity>()
    // createComposeRule() if you don't need access to the activityTestRule

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun MyTest() {
        composeTestRule.setContent {
            SampleAppTheme {
                DiyDietPlanPage(){}
            }
        }
    }

}