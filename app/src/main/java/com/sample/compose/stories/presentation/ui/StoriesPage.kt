package com.sample.compose.stories.presentation.ui

import android.util.Log
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.sample.compose.stories.presentation.viewmodel.StoriesViewModel

@Composable
fun StoriesPage(
    modifier: Modifier = Modifier,
    viewModel: StoriesViewModel = viewModel(),
    onBackClick: () -> Unit,
) {

    val uiModel by viewModel.uiModel

    if (uiModel.isPageReady) {
        val stories = uiModel.stories

        Box(modifier = modifier.fillMaxSize()) {

            var currentStoryPosition by remember { mutableStateOf(0) }
            var isPressed by remember { mutableStateOf(false) }

            Image(
                modifier = Modifier
                    .fillMaxSize()
                    .pointerInput(Unit) {
                        detectTapGestures(
                            onPress = {
                                isPressed = true
                                tryAwaitRelease()
                                isPressed = false
                            }
                        )
                    },
                painter = rememberImagePainter(data = stories[currentStoryPosition].imageUrl),
                contentDescription = null,
                contentScale = ContentScale.FillBounds
            )

            StoriesProgressBarRow(
                modifier = Modifier
                    .height(56.dp)
                    .fillMaxWidth()
                    .background(
                        brush = Brush.verticalGradient(
                            listOf(Color.DarkGray, Color.Transparent)
                        )
                    ),
                count = stories.size,
                currentStoryPosition = currentStoryPosition,
                paused = isPressed,
                onCurrentProgressFinished = {
                    if (currentStoryPosition == stories.lastIndex) {
                        onBackClick()
                        return@StoriesProgressBarRow
                    }
                    currentStoryPosition++
                }
            )
        }
    } else {
        Box(modifier = modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            CircularProgressIndicator()
        }
    }
}

@Composable
private fun StoriesProgressBarRow(
    count: Int,
    currentStoryPosition: Int,
    onCurrentProgressFinished: () -> Unit,
    modifier: Modifier = Modifier,
    paused: Boolean = false,
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        for (i in 0 until count) {
            Spacer(modifier = Modifier.width(4.dp))
            StoriesProgressBarV2(
                modifier = Modifier.weight(1f),
                isActive = i == currentStoryPosition,
                nonActiveProgress = if (i > currentStoryPosition) 0f else 1f,
                paused = paused
            ) {
                onCurrentProgressFinished()
            }
            if (i == count - 1) {
                Spacer(modifier = Modifier.width(4.dp))
            }
        }
    }
}

@Composable
private fun StoriesProgressBar(
    isActive: Boolean,
    nonActiveProgress: Float,
    modifier: Modifier = Modifier,
    totalSecondsInMillis: Int = 3000,
    onProgressFinished: () -> Unit = {},
) {

    var animationIsRunning by remember { mutableStateOf(false) }

    val progress = if (isActive) animateFloatAsState(
        targetValue = if (animationIsRunning) 1f else 0f,
        animationSpec = tween(durationMillis = totalSecondsInMillis, easing = LinearEasing),
        finishedListener = { onProgressFinished() }
    ).value
    else nonActiveProgress

    LaunchedEffect(isActive) {
        if (isActive) {
            animationIsRunning = !animationIsRunning
        }
    }

    Log.d("compose_tag", "Progress : $progress")

    LinearProgressIndicator(
        progress = progress,
        modifier = modifier.clip(shape = RoundedCornerShape(8.dp)),
        backgroundColor = Color.LightGray,
        color = Color.White
    )
}

@Composable
private fun StoriesProgressBarV2(
    isActive: Boolean,
    nonActiveProgress: Float,
    modifier: Modifier = Modifier,
    paused: Boolean = false,
    totalSecondsInMillis: Int = 3000,
    onProgressFinished: () -> Unit = {},
) {

    val percent = remember { Animatable(0f) }

    LaunchedEffect(isActive, paused) {
        if (isActive) {
            if (paused) percent.stop()
            else {
                percent.animateTo(
                    targetValue = 1f,
                    animationSpec = tween(
                        durationMillis = (totalSecondsInMillis * (1f - percent.value)).toInt(),
                        easing = LinearEasing
                    )
                )
                onProgressFinished()
            }
        } else {
            percent.stop()
        }
    }

    Log.d("compose_tag", "Progress : $percent.value")

    LinearProgressIndicator(
        progress = if (isActive) percent.value else nonActiveProgress,
        modifier = modifier.clip(shape = RoundedCornerShape(8.dp)),
        backgroundColor = Color.LightGray,
        color = Color.White
    )
}