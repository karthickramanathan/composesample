package com.sample.compose.stories.presentation.viewmodel

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sample.compose.stories.model.Stories
import com.sample.compose.stories.model.StoriesUiModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class StoriesViewModel : ViewModel() {

    private val _uiModel = mutableStateOf(StoriesUiModel(isPageReady = false, stories = listOf()))
    val uiModel: State<StoriesUiModel> = _uiModel

    private var stories = listOf<Stories>()

    init {
        getPageReady()
    }

    private fun getPageReady() {
        viewModelScope.launch {
            delay(500)
            stories = getData()
            _uiModel.value = StoriesUiModel(true, stories)
        }
    }

    private fun getData() = listOf(
        Stories(id = 1, imageUrl = "https://wallpapercave.com/wp/wp3003500.jpg"),
        Stories(id = 2, imageUrl = "https://wallpapercave.com/wp/wp2663986.png"),
        Stories(id = 3, imageUrl = "https://wallpaperaccess.com/full/2267741.jpg"),
        Stories(
            id = 4,
            imageUrl = "https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto,q_auto,f_auto/gigs/134312612/original/9c9fe0b23d955e935df4905dcfdd7685fa025cb0/top-100-best-black-panther-wallpapers-for-mobile.jpg"
        ),
        Stories(
            id = 5,
            imageUrl = "https://w0.peakpx.com/wallpaper/139/881/HD-wallpaper-black-panther-black-panther-chadwick-boseman-dark-marvel-wakanda-forever.jpg"
        )
    )
}