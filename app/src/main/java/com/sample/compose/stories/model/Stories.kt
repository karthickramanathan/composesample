package com.sample.compose.stories.model

data class StoriesUiModel(
    val isPageReady: Boolean,
    val stories: List<Stories>,
)

data class Stories(
    val id: Int,
    val imageUrl: String,
)
