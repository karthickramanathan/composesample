package com.sample.compose.base.utils

import androidx.compose.ui.graphics.Color


fun String?.parseARGBComposeColor(
    defaultColor: Color
): Color {
    return try {
        Color(android.graphics.Color.parseColor(this))
    } catch (e: Exception) {
        e.printStackTrace()
        defaultColor
    }
}