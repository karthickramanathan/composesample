package com.sample.compose.base.utils

import androidx.compose.ui.text.capitalize
import java.util.*

private val camelCaseRegex = "(?<=[a-zA-Z])[A-Z]".toRegex()
private val snakeCaseRegex = "_[a-zA-Z]".toRegex()

fun String.convertSnakeCaseToLowerCamelCase(underScoreReplacement: Char): String {
    return snakeCaseRegex.replace(this) { it.value.replace('_', underScoreReplacement) }
}

fun String.convertSnakeCaseToUpperCamelCase(underScoreReplacement: Char): String {
    return convertSnakeCaseToLowerCamelCase(underScoreReplacement)
        .replaceFirstChar {
            if (it.isLowerCase()) it.titlecase(Locale.getDefault())
            else it.toString()
        }
}
