package com.sample.compose.skubackpress.presentation.viewmodel

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.sample.compose.skubackpress.data.model.PlanItemUiModel

private val samplePlanItems = listOf(
    PlanItemUiModel(
        months = 12,
        fullAmountMonth = 399,
        discountAmountMonth = 199,
        isRecommended = true,
    ),
    PlanItemUiModel(
        months = 6,
        fullAmountMonth = 499,
        discountAmountMonth = 299,
        isRecommended = false,
    ),
)

class SkuBackPressViewModel : ViewModel() {

    private val _plans: MutableState<List<PlanItemUiModel>> = mutableStateOf(samplePlanItems)
    val plans: State<List<PlanItemUiModel>> = _plans

    private val _selectedPlan: MutableState<PlanItemUiModel> = mutableStateOf(samplePlanItems[0])
    val selectedPlan: State<PlanItemUiModel> = _selectedPlan

    fun onPlanSelected(plan: PlanItemUiModel) {
        _selectedPlan.value = plan
    }

}