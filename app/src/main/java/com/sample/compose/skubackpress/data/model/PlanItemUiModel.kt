package com.sample.compose.skubackpress.data.model

data class PlanItemUiModel(
    val months: Int,
    val fullAmountMonth: Int,
    val discountAmountMonth: Int,
    val isRecommended: Boolean,
    val currency: Char = '₹',
)