package com.sample.compose.skubackpress.presentation.ui

import androidx.compose.animation.*
import androidx.compose.animation.core.*
import androidx.compose.animation.core.FastOutLinearInEasing
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.CheckCircle
import androidx.compose.material.icons.rounded.RadioButtonUnchecked
import androidx.compose.material.icons.rounded.Schedule
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.compositeOver
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.viewmodel.compose.viewModel
import com.sample.compose.R
import com.sample.compose.components.countdowntimer.CountDownTimerState
import com.sample.compose.components.coupon.CouponCard
import com.sample.compose.skubackpress.data.model.PlanItemUiModel
import com.sample.compose.skubackpress.presentation.viewmodel.SkuBackPressViewModel
import kotlinx.coroutines.delay


@Composable
fun SkuBackPress(
    modifier: Modifier = Modifier,
    contentModifier: Modifier = Modifier,
    viewModel: SkuBackPressViewModel = viewModel(),
    onBackClick: () -> Unit
) {

    Scaffold(modifier = modifier) { paddingValues ->

        Column(
            modifier = contentModifier.padding(16.dp)
        ) {
            DiscountCard(
                discountPercentage = 29,
                modifier = Modifier
                    .zIndex(1f)
                    .fillMaxWidth()
                    .height(200.dp),
            )
            CounterItem(
                modifier = Modifier
                    .fillMaxWidth()
                    .heightIn(min = 24.dp)
                    .offset(y = (-12).dp),
                countDown = 30L,
                timerExpired = onBackClick,
            )
            //Spacer(modifier = Modifier.height(16.dp))
            Text(
                text = "Choose a plan to Lose 20 kgs 2x faster",
                color = Color.Black,
                fontWeight = FontWeight.Bold
            )
            Spacer(modifier = Modifier.height(16.dp))

            val plans = viewModel.plans.value
            val selectedPlan = viewModel.selectedPlan.value

            plans.forEachIndexed { index, plan ->
                PlanItem(
                    plan = plan,
                    selected = plan == selectedPlan,
                    onClick = viewModel::onPlanSelected
                )
                val spaceHeight = if (index > 0 && plans[index - 1].isRecommended) 16.dp else 8.dp
                Spacer(modifier = Modifier.height(spaceHeight))
            }
        }
    }
}

@Composable
private fun DiscountCard(modifier: Modifier = Modifier, discountPercentage: Int) {

    CouponCard(
        elevation = 4.dp,
        backgroundColor = Color(0xFFFFEEED),
        modifier = modifier
    ) {
        Box(modifier = Modifier.padding(16.dp)) {
            Image(
                painter = painterResource(id = R.drawable.discount_coupon_stars),
                modifier = Modifier.align(Alignment.CenterEnd),
                contentDescription = null
            )
            Image(
                painter = painterResource(id = R.drawable.discount_coupon_gift),
                modifier = Modifier
                    .fillMaxHeight()
                    .align(Alignment.CenterEnd),
                contentDescription = null
            )
            Column(
                modifier = Modifier
                    .fillMaxSize(),
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "Special Discount",
                    modifier = Modifier
                        .background(
                            color = Color(0xFFFEE2E0),
                            shape = RoundedCornerShape(12.dp)
                        )
                        .padding(horizontal = 16.dp, vertical = 4.dp),
                    fontWeight = FontWeight.Bold,
                    color = Color.Red,
                    fontSize = 12.sp
                )
                Text(
                    text = "$discountPercentage% OFF",
                    modifier = Modifier,
                    color = Color.Red,
                    fontSize = 32.sp
                )
                Text(text = "for you on HealthifySmart!", color = Color.Red, fontSize = 16.sp)
            }
        }
    }
}


@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun CounterItem(
    modifier: Modifier = Modifier,
    countDown: Long,
    timerExpired: () -> Unit,
) {
    var visibleState by remember { mutableStateOf(false) }

    /*val countDownTime: Long by rememberCountDownTimer(
        startDelayInMillis = 2000L,
        maxTimeInMillis = 30 * 1000L,
        onTimerFinished = timerExpired
    )*/

    val timerState = remember { CountDownTimerState(30 * 1000L) }

    timerState.onTimerFinished = timerExpired

    val lifecycle = LocalLifecycleOwner.current.lifecycle
    val lifecycleEventObserver = remember {
        LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_STOP) {
                timerState.cancelTimer()
            }
        }
    }

    DisposableEffect(timerState) {
        onDispose { timerState.onDispose() }
    }

    DisposableEffect(lifecycle) {
        lifecycle.addObserver(lifecycleEventObserver)
        onDispose { lifecycle.removeObserver(lifecycleEventObserver) }
    }

    LaunchedEffect(countDown) {
        delay(2000L)
        visibleState = true
        timerState.startTimer()
    }

    Box(
        modifier = modifier
            .background(
                color = Color(0xFFEF4437),
                shape = RoundedCornerShape(bottomStart = 8.dp, bottomEnd = 8.dp)
            )
    ) {
        AnimatedVisibility(
            visible = visibleState,
            enter = expandVertically()
        ) {
            Row(
                modifier = Modifier
                    .height(56.dp)
                    .align(Alignment.BottomCenter)
                    .padding(10.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.Bottom
            ) {
                Text(
                    text = "This offer expires in",
                    color = Color.White,
                    modifier = Modifier.weight(1f)
                )
                Icon(
                    imageVector = Icons.Rounded.Schedule,
                    contentDescription = null,
                    tint = Color.White
                )
                Spacer(modifier = Modifier.width(4.dp))
                AnimatedNumber(targetState = timerState.remainingTime.value / 1000L)
            }
        }
    }
}

@Composable
private fun PlanItem(
    modifier: Modifier = Modifier,
    plan: PlanItemUiModel,
    selected: Boolean,
    onClick: (PlanItemUiModel) -> Unit
) {

    val strokeColor = if (selected) Color(0xFF8BC249) else Color(0xFFDFDFDF)
    val backgroundColor =
        if (selected) Color(0xFF8BC249).copy(alpha = 0.2f).compositeOver(Color.White)
        else Color(0xFFFCFCFC)

    val savedAmount = plan.fullAmountMonth - plan.discountAmountMonth
    val currency = plan.currency
    val discountAmount = plan.discountAmountMonth
    val fullAmount = plan.fullAmountMonth

    Box(modifier = modifier) {

        ConstraintLayout {

            val (recommendText, card) = createRefs()

            Card(
                modifier = Modifier
                    .constrainAs(card) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                    }
                    .clickable { onClick(plan) },
                border = BorderStroke(width = 2.dp, color = strokeColor),
                shape = RoundedCornerShape(8.dp),
                backgroundColor = backgroundColor
            ) {
                Row(
                    modifier = Modifier.padding(
                        vertical = if (plan.isRecommended) 36.dp else 24.dp,
                        horizontal = 16.dp
                    ),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        imageVector = if (selected) Icons.Rounded.CheckCircle
                        else Icons.Rounded.RadioButtonUnchecked,
                        contentDescription = null,
                        tint = if (selected) Color(0xFF8BC249) else Color.Gray
                    )
                    Spacer(modifier = Modifier.width(16.dp))
                    Column(
                        modifier = Modifier.weight(1f)
                    ) {
                        Text(text = "${plan.months} months", fontWeight = FontWeight.Bold)
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(text = "at ${plan.currency}$savedAmount/m")
                    }
                    Column(
                        horizontalAlignment = Alignment.End
                    ) {
                        Row {
                            Text(
                                text = "$currency$fullAmount/m",
                                color = Color.LightGray,
                                textDecoration = TextDecoration.LineThrough,
                            )
                            Spacer(modifier = Modifier.width(8.dp))
                            Text(
                                text = "$currency$discountAmount/m",
                                fontWeight = FontWeight.Bold
                            )
                        }
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(text = "Total $currency${discountAmount * plan.months}")
                    }
                }
            }
            if (plan.isRecommended)
                Text(
                    text = "Recommended",
                    color = Color.White,
                    fontSize = 11.sp,
                    modifier = Modifier
                        .zIndex(1f)
                        .constrainAs(recommendText) {
                            top.linkTo(card.top)
                            bottom.linkTo(card.top)
                            end.linkTo(card.end)
                        }
                        .padding(end = 16.dp)
                        .background(
                            color = strokeColor,
                            shape = RoundedCornerShape(4.dp)
                        )
                        .padding(horizontal = 8.dp, vertical = 4.dp)
                )
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun AnimatedNumber(targetState: Long, modifier: Modifier = Modifier) {
    AnimatedContent(
        modifier = modifier,
        targetState = targetState,
        transitionSpec = {
            // Compare the incoming number with the previous number.
            if (targetState > initialState) {
                // If the target number is larger, it slides up and fades in
                // while the initial (smaller) number slides up and fades out.
                slideInVertically({ height -> height }) + fadeIn() with
                        slideOutVertically({ height -> -height }) + fadeOut()
            } else {
                // If the target number is smaller, it slides down and fades in
                // while the initial number slides down and fades out.
                slideInVertically({ height -> -height }) + fadeIn() with
                        slideOutVertically({ height -> height }) + fadeOut()
            }.using(
                // Disable clipping since the faded slide-in/out should
                // be displayed out of bounds.
                SizeTransform(clip = false)
            )
        }
    ) { targetCount ->
        Text(
            text = "${targetCount}s",
            color = Color.White
        )
    }
}

