package com.sample.compose.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable


private val ThemeLightColors = lightColors(
    primary = colorLightPrimary,
    primaryVariant = colorLightPrimaryVariant,
    onPrimary = colorLightOnPrimary,
    secondary = colorLightPrimaryDark,
    secondaryVariant = colorLightAccent,
    onSecondary = colorLightOnPrimary,
    background = colorLightBackground,
    onBackground = colorLightOnBackground,
    surface = colorLightSurface,
    onSurface = colorLightOnSurface,
    error = colorLightError,
    onError = colorLightOnError
)

private val ThemeDarkColors = darkColors(
    primary = colorDarkPrimary,
    primaryVariant = colorDarkPrimaryVariant,
    onPrimary = colorDarkOnPrimary,
    secondary = colorDarkPrimaryDark,
    secondaryVariant = colorDarkAccent,
    onSecondary = colorDarkOnPrimary,
    background = colorDarkBackground,
    onBackground = colorDarkOnBackground,
    surface = colorDarkSurface,
    onSurface = colorDarkOnSurface,
    error = colorDarkError,
    onError = colorDarkOnError
)

@Composable
fun SampleAppTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) ThemeDarkColors else ThemeLightColors

    MaterialTheme(
        colors = colors,
        content = content
    )
}