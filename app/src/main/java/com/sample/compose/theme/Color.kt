package com.sample.compose.theme

import androidx.compose.ui.graphics.Color


val colorLightPrimary = Color(0xFFD50000)
val colorLightPrimaryDark  = Color(0xFFB80101)
val colorLightAccent  = Color(0xFFD50000)
val colorLightOnPrimary  = Color(0xFFFFFFFF)
val colorLightError  = Color(0xFFAA0000)
val colorLightOnError  = Color(0xFFFFFFFF)
val colorLightSurface  = Color(0xFFFFFFFF)
val colorLightOnSurface  = Color(0xFF333333)
val colorLightBackground  = Color(0xFFF6F6F6)
val colorLightOnBackground  = Color(0xFF333333)
val colorLightPrimaryVariant  = Color(0xFFF8D6D6)

val colorLightDisabled = Color(0xFF9D9D9D)
val colorLightDisabledText = Color(0xFFEEEEEE)
val colorLightSeparatorColor = Color(0xFFF6F6F6)
val colorLightSeparatorLineColor = Color(0xFFEEEEEE)

val colorDarkPrimary = Color(0xFFFF5252)
val colorDarkPrimaryDark  = Color(0xFFFF5252)
val colorDarkAccent  = Color(0xFFFF5252)
val colorDarkOnPrimary  = Color(0xFF121212)
val colorDarkError  = Color(0xFFFF80AB)
val colorDarkOnError  = Color(0xFFFFFFFF)
val colorDarkSurface  = Color(0xFF121212)
val colorDarkOnSurface  = Color(0xDEFFFFFF)
val colorDarkBackground  = Color(0xFF121212)
val colorDarkOnBackground  = Color(0xDEFFFFFF)
val colorDarkPrimaryVariant  = Color(0xFF381C1C)

val colorDarkDisabled = Color(0xFF9D9D9D)
val colorDarkDisabledText = Color(0xFFEEEEEE)
val colorDarkSeparatorColor = Color(0xFF000000)
val colorDarkSeparatorLineColor = Color(0x33FFFFFF)

val colorRiaInsightGreen = Color(0xFF8BC249)
val colorRiaInsightYellow = Color(0xFFFBC934)
val colorRiaInsightRed = Color(0xFFEF4437)

val colorRiaInsightCarb = Color(0xFFFF7701)
val colorRiaInsightFat = Color(0xFFFFAD66)
val colorRiaInsightProtein = Color(0xFFFFD1AA)
val colorRiaInsightLightOrange = Color(0xFFFFF1E5)

val colorGrey = Color(0xFFE5E5E5)