package com.sample.compose

import androidx.compose.runtime.Composable
import com.google.accompanist.insets.ProvideWindowInsets
import com.sample.compose.navigation.SampleNavGraph
import com.sample.compose.theme.SampleAppTheme

const val COMPOSE_TAG = "compose_log"

@Composable
fun SampleApp() {
    ProvideWindowInsets {
        SampleAppTheme {
            SampleNavGraph()
        }
    }
}