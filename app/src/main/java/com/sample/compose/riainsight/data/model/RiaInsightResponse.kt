package com.sample.compose.riainsight.data.model


import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

class RiaInsightResponse(
    @SerializedName("insights")
    var insights: List<Insight>,
    @SerializedName("should_show_lock_state")
    var shouldShowLockState: Boolean,
    @SerializedName("insights_details_cta_text")
    var insightDetailsCtaText: String?,
    @SerializedName("lock_state_cta")
    var lockStateCta: String?,
    @SerializedName("lock_state_cta_text")
    var lockStateCtaText: String?,
    @SerializedName("lock_state_title")
    var lockStateTitle: String?,
    @SerializedName("lock_state_subtext")
    var lockStateSubtext: String?
)

class Insight(
    @SerializedName("speech")
    var speech: String,
    @SerializedName("displayText")
    var displayText: String,
    @SerializedName("data")
    var dataDetails: ViewData
)

class ViewData(
    @SerializedName("extras")
    var extras: Extras,
    @SerializedName("followup_type")
    var followupType: Int,
    @SerializedName("response_mode")
    var responseMode: Int,
    @SerializedName("followup_style")
    var followupStyle: Int,
    @SerializedName("should_show_rating")
    var shouldShowRating: Boolean
)

class Extras(
    @SerializedName("native_view")
    var nativeView: NativeView
)

class NativeView(
    @SerializedName("view_type")
    var viewType: Int,
    @SerializedName("parameters")
    var parameters: JsonObject
)

const val KEY_INSIGHTS_INFO = "insights_info"
const val KEY_EXTRA_INFO = "extra_info"
const val KEY_IS_SUMMARY_CARD = "is_summary_card"