package com.sample.compose.riainsight.presentation.ui.components

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ChevronRight
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sample.compose.theme.colorRiaInsightRed

@Composable
fun RiaCtaButton(
    text: String,
    modifier: Modifier = Modifier,
    onClick: () -> Unit = {},
) {
    TextButton(
        modifier = modifier,
        shape = RectangleShape,
        onClick = onClick,
    ) {
        Text(
            modifier = Modifier.weight(1f),
            text = text,
            color = colorRiaInsightRed,
            fontSize = 14.sp
        )
        Icon(
            imageVector = Icons.Rounded.ChevronRight,
            contentDescription = null,
            tint = colorRiaInsightRed
        )
    }
}