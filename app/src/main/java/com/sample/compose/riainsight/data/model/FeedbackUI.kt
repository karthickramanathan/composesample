package com.sample.compose.riainsight.data.model

class FeedbackUI(
    val feedbackQuestion: String?,
    val positiveCta: String?,
    val negativeCta: String?
    )