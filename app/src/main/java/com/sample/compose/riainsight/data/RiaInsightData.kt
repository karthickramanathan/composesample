package com.sample.compose.riainsight.data


object RiaInsightData {
    val data1 = """
        {
            "insight_type":1,
            "insights":[
                {
                    "data":{
                        "extras":{
                            "native_view":{
                                "parameters":{
                                    "actions":null,
                                    "body_text":"Your calorie intake for Dinner is in control, but there's scope to improve! I have recommendations to balance your carb intake better.",
                                    "header_text":"Hi Karthick!",
                                    "icon_url":null,
                                    "insights_info":{
                                        "extra_info":{
                                            "element_id":"955",
                                            "insight_card_id":"58",
                                            "insight_card_rule_map_id":"58",
                                            "insight_card_rule_map_log_id":2667102,
                                            "insight_element_log_id":17025966,
                                            "is_summary_card":false,
                                            "rule_name":"cal_normal_carb_high"
                                        },
                                        "feedback_key":"1-cid:58-eid:955"
                                    },
                                    "summary_text":"Wonderful ✨ Your calorie intake for Dinner is on point! I can help you make changes to your diet to balance your carb intake better. Read on to know more 👉"
                                },
                                "view_type":18
                            }
                        },
                        "followup_style":1,
                        "followup_type":2,
                        "response_mode":2,
                        "should_show_rating":true
                    },
                    "displayText":"",
                    "speech":""
                },
                {
                    "data":{
                        "extras":{
                            "native_view":{
                                "parameters":{
                                    "header_text":"Dinner Calories",
                                    "insights_info":{
                                        "extra_info":{
                                            "element_id":"956",
                                            "insight_card_id":"58",
                                            "insight_card_rule_map_id":"58",
                                            "insight_card_rule_map_log_id":2667102,
                                            "insight_element_log_id":17025968,
                                            "is_summary_card":true,
                                            "rule_name":"cal_normal_carb_high"
                                        },
                                        "feedback_key":"1-cid:58-eid:956"
                                    },
                                    "macro_budget":"328",
                                    "macro_consumed":"303",
                                    "macro_fill_colour":"#8BC249",
                                    "macro_info":"You have a 8% calorie deficit today",
                                    "summary":"Excellent! You've balanced your calories for Dinner 👏 I'm here to help keep you on track! Read on for more"
                                },
                                "view_type":19
                            }
                        },
                        "followup_style":1,
                        "followup_type":2,
                        "response_mode":2,
                        "should_show_rating":true
                    },
                    "displayText":"",
                    "speech":""
                },
                {
                    "data":{
                        "extras":{
                            "native_view":{
                                "parameters":{
                                    "consumed_pfc_ratio":{
                                        "carbs":80,
                                        "fats":4,
                                        "protein":16
                                    },
                                    "goal_pfc_ratio":{
                                        "carbs":50,
                                        "fats":30,
                                        "protein":20
                                    },
                                    "header_background_color":"#FFF1ES",
                                    "header_text":"Dinner Macronutrients Ratio",
                                    "insights_info":{
                                        "extra_info":{
                                            "element_id":"994",
                                            "insight_card_id":"58",
                                            "insight_card_rule_map_id":"58",
                                            "insight_card_rule_map_log_id":2667102,
                                            "insight_element_log_id":17025970,
                                            "is_summary_card":false,
                                            "rule_name":"cal_normal_carb_high"
                                        },
                                        "feedback_key":"1-cid:58-eid:994"
                                    },
                                    "nutrition_info_map":{
                                        "carbs":{
                                            "budget":40.97541646957397,
                                            "consumed":58.49040091728404,
                                            "status":2
                                        },
                                        "fats":{
                                            "budget":10.926777725219726,
                                            "consumed":2.680304057367092,
                                            "status":0
                                        },
                                        "protein":{
                                            "budget":16.39016658782959,
                                            "consumed":11.27627781311223,
                                            "status":1
                                        }
                                    },
                                    "summary":"Seems like your Dinner was high in carb. Let's take a look at your macronutrient breakdown."
                                },
                                "view_type":20
                            }
                        },
                        "followup_style":1,
                        "followup_type":2,
                        "response_mode":2,
                        "should_show_rating":true
                    },
                    "displayText":"True",
                    "speech":"True"
                },
                {
                    "data":{
                        "extras":{
                            "native_view":{
                                "parameters":{
                                    "summary":"Your breakfast was low in protein and high in carbs - not ideal for losing that weight.",
                                    "calorie_consumed":171.0,
                                    "calorie_budget":328.0,
                                    "carb":57.9,
                                    "carb_budget":256.0,
                                    "protein":25.9,
                                    "protein_budget":20.5,
                                    "fat":47.9,
                                    "fat_budget":68.3,
                                    "fibre":30.5,
                                    "fibre_budget":30.5,
                                    "header_text":"Breakfast Macronutrients breakup",
                                    "header_background_color":"#FFF1E5",
                                    "insights_info":{
                                        "extra_info":{
                                            "element_id":"993",
                                            "insight_card_id":"58",
                                            "insight_card_rule_map_id":"58",
                                            "insight_card_rule_map_log_id":2667102,
                                            "insight_element_log_id":17025975,
                                            "is_summary_card":false,
                                            "rule_name":"cal_normal_carb_high"
                                        },
                                        "feedback_key":"1-cid:58-eid:993"
                                    }
                                },
                                "view_type":0
                            }
                        },
                        "followup_style":1,
                        "followup_type":2,
                        "response_mode":2,
                        "should_show_rating":true
                    },
                    "displayText":"",
                    "speech":""
                },
                {
                    "data":{
                        "extras":{
                            "native_view":{
                                "parameters":{
                                    "calorie_budget":353,
                                    "foods":[
                                        {
                                            "calories":116,
                                            "food_info_colour":"#8BC249",
                                            "id":32,
                                            "name":"Red Gram dal",
                                            "positive_feedback":"This was perfect, good job!",
                                            "qty":"You had - 1.0 katori <b>·</b> 116 cal",
                                            "recommended_qty_info":""
                                        },
                                        {
                                            "calories":24,
                                            "food_info_colour":"#8BC249",
                                            "id":10406,
                                            "name":"Sprouts chaat",
                                            "positive_feedback":"",
                                            "qty":"You had - 1.5 katori <b>·</b> 35 cal",
                                            "recommended_qty_info":"Recommended - 1.0 katori"
                                        },
                                        {
                                            "calories":68,
                                            "food_info_colour":"#8BC249",
                                            "id":9161,
                                            "name":"Bhindi Bhaji",
                                            "positive_feedback":"This was perfect, good job!",
                                            "qty":"You had - 1.0 katori <b>·</b> 68 cal",
                                            "recommended_qty_info":""
                                        },
                                        {
                                            "calories":85,
                                            "food_info_colour":"#8BC249",
                                            "id":22,
                                            "name":"Roti",
                                            "positive_feedback":"This was perfect, good job!",
                                            "qty":"You had - 1.0 roti/chapati <b>·</b> 85 cal",
                                            "recommended_qty_info":""
                                        },
                                        {
                                            "calories":60,
                                            "food_info_colour":"#8BC249",
                                            "id":29,
                                            "name":"Plain Cooked Rice",
                                            "positive_feedback":"",
                                            "qty":"",
                                            "recommended_qty_info":"Recommended - 0.5 katori"
                                        }
                                    ],
                                    "header_text":"Revised Lunch Portions",
                                    "insights_info":{
                                        "extra_info":{
                                            "element_id":"995",
                                            "insight_card_id":"3",
                                            "is_summary_card":false,
                                            "rule_name":"cal_high_carb_high"
                                        },
                                        "feedback_key":"1-cid:3-eid:995"
                                    },
                                    "ria_cta":"hmein://activity/Assistant?auto_send=True&question_prefill=Healthy+Meal+Options&answer_intent=dislike-odd-combination meal&should_call_initiator=False&should_call_resume=False&cta_source=insights-v2-food-tracking&cta_tag=Other_food_options&should_finish=true&meal_type=L",
                                    "ria_cta_text":"GET MORE SUGGESTIONS FROM RIA",
                                    "summary_text":"I like the Lunch that you ate. But including excess of Plain Cooked Rice made it calorie-heavy. I just played around with the portions to make it healthier for your goal. Give it a try!"
                                },
                                "view_type":23
                            }
                        },
                        "followup_style":1,
                        "followup_type":2,
                        "response_mode":2,
                        "should_show_rating":true
                    },
                    "displayText":"",
                    "speech":""
                },
                {
                    "data":{
                        "extras":{
                            "native_view":{
                                "parameters":{
                                    "calorie_budget":328,
                                    "foods":[
                                        {
                                            "calories":171,
                                            "id":22,
                                            "name":"Roti",
                                            "qty":"2.0 roti/chapati"
                                        },
                                        {
                                            "calories":83,
                                            "id":27798,
                                            "name":"Broccoli Capsicum Paneer Subzi",
                                            "qty":"0.5 katori"
                                        }
                                    ],
                                    "header_text":"Dinner Suggestion",
                                    "insights_info":{
                                        "extra_info":{
                                            "element_id":"993",
                                            "insight_card_id":"58",
                                            "insight_card_rule_map_id":"58",
                                            "insight_card_rule_map_log_id":2667102,
                                            "insight_element_log_id":17025975,
                                            "is_summary_card":false,
                                            "rule_name":"cal_normal_carb_high"
                                        },
                                        "feedback_key":"1-cid:58-eid:993"
                                    },
                                    "ria_cta":"hmein://activity/Assistant?auto_send=True&question_prefill=Healthy+Meal+Options&answer_intent=dislike-odd-combination-meal&should_call_initiator=False&should_call_resume=False&cta_source=insights_v2&cta_tag=card_id%3A8%3A%3Aelement_id%3A993&insight_element_log_id=17025975&should_finish=true&meal_type=D",
                                    "ria_cta_text":"GET MORE SUGGESTIONS FROM RIA",
                                    "summary_text":"Good choice of a meal! Here is a similar meal suggestion for you."
                                },
                                "view_type":24
                            }
                        },
                        "followup_style":1,
                        "followup_type":2,
                        "response_mode":2,
                        "should_show_rating":true
                    },
                    "displayText":"",
                    "speech":""
                },
                {
                    "data":{
                        "extras":{
                            "native_view":{
                                "parameters":{
                                    "feedback_answer_placeholder":"Type your feedback here.",
                                    "feedback_question_text":"Anything else you want us to improve?",
                                    "insights_info":{
                                        "extra_info":{
                                            "element_id":"972",
                                            "insight_card_id":"58",
                                            "insight_card_rule_map_id":"58",
                                            "insight_card_rule_map_log_id":2667102,
                                            "insight_element_log_id":17025977,
                                            "is_summary_card":false,
                                            "rule_name":"cal_normal_carb_high"
                                        },
                                        "feedback_key":"1-cid:58-eid:972"
                                    },
                                    "question_text":"Karthick, did you find these insights helpful?",
                                    "title":"I'd love some feedback from you..."
                                },
                                "view_type":22
                            }
                        },
                        "followup_style":1,
                        "followup_type":2,
                        "response_mode":2,
                        "should_show_rating":true
                    },
                    "displayText":"",
                    "speech":""
                },
                {
                    "data":{
                        "extras":{
                            "native_view":{
                                "parameters":{
                                    "foods":[
                                        {
                                            "calories":326,
                                            "id":188,
                                            "macro_info":"36gm Protein",
                                            "macro_info_color":"#8BC249",
                                            "name":"Curry Chicken",
                                            "qty":"2.00 katori",
                                            "summary":"Your protein intake is impressive. Here's the food you tracked. Keep up the good work!",
                                            "alternatives":[
                                                {
                                                    "calories":"82",
                                                    "id":188,
                                                    "macro_info":"4gm Fat",
                                                    "macro_info_color":"#8BC249",
                                                    "name":"Curry Chicken",
                                                    "qty":"0.50 katori"
                                                }
                                            ],
                                            "alternatives_section_title":"Recommended portions"
                                        },
                                        {
                                            "calories":171,
                                            "id":22,
                                            "macro_info":"6gm Protein",
                                            "macro_info_color":"#8BC249",
                                            "name":"Sapathi",
                                            "qty":"2.00 roti/chapati",
                                            "summary":"Your protein intake is impressive. Here's the food you tracked. Keep up the good work!"
                                        }
                                    ],
                                    "header_background_color":"#EDF7E1",
                                    "header_text":"High protein Foods had during Lunch",
                                    "insights_info":{
                                        "extra_info":{
                                            "element_id":"979",
                                            "insight_card_id":"4",
                                            "insight_card_rule_map_id":"4",
                                            "insight_card_rule_map_log_id":3257171,
                                            "insight_element_log_id":20492941,
                                            "is_summary_card":false,
                                            "rule_name":"cal_high_protein_high"
                                        },
                                        "feedback_key":"1-cid:4-eid:979"
                                    },
                                    "ria_cta":"hmein://activity/Assistant?auto_send=True&question_prefill=Healthy+Meal+Options&answer_intent=dislike-odd-combination-meal&should_call_initiator=False&should_call_resume=False&cta_source=insights-v2-food-tracking&cta_tag=Other_food_options&should_finish=true&meal_type=L",
                                    "ria_cta_text":"GET MORE SUGGESTIONS FROM RIA"
                                },
                                "view_type":21
                            }
                        },
                        "followup_style":1,
                        "followup_type":2,
                        "response_mode":2,
                        "should_show_rating":true
                    },
                    "displayText":"",
                    "speech":""
                },
                {
                    "data":{
                        "extras":{
                            "native_view":{
                                "parameters":{
                                    "foods":[
                                        {
                                            "calories":171,
                                            "id":22,
                                            "macro_info":"5gm Fibre",
                                            "macro_info_color":"#8BC249",
                                            "name":"Sapathi",
                                            "qty":"2.00 roti/chapati",
                                            "summary":"This food is the highlight of your day today 🤩 Keep eating such healthy foods!"
                                        },
                                        {
                                            "calories":326,
                                            "id":188,
                                            "macro_info":"3gm Fibre",
                                            "macro_info_color":"#8BC249",
                                            "name":"Curry Chicken",
                                            "qty":"2.00 katori",
                                            "summary":"This food is the highlight of your day today 🤩 Keep eating such healthy foods!"
                                        }
                                    ],
                                    "header_background_color":"#EDF7E1",
                                    "header_text":"Balanced fibre Foods had during Lunch",
                                    "insights_info":{
                                        "extra_info":{
                                            "element_id":"978",
                                            "insight_card_id":"4",
                                            "insight_card_rule_map_id":"4",
                                            "insight_card_rule_map_log_id":3257171,
                                            "insight_element_log_id":20492942,
                                            "is_summary_card":false,
                                            "rule_name":"cal_high_protein_high"
                                        },
                                        "feedback_key":"1-cid:4-eid:978"
                                    },
                                    "ria_cta":"hmein://activity/Assistant?auto_send=True&question_prefill=Healthy+Meal+Options&answer_intent=dislike-odd-combination-meal&should_call_initiator=False&should_call_resume=False&cta_source=insights-v2-food-tracking&cta_tag=Other_food_options&should_finish=true&meal_type=L",
                                    "ria_cta_text":"GET MORE SUGGESTIONS FROM RIA"
                                },
                                "view_type":21
                            }
                        },
                        "followup_style":1,
                        "followup_type":2,
                        "response_mode":2,
                        "should_show_rating":true
                    },
                    "displayText":"",
                    "speech":""
                }
            ],
            "insights_details_cta_text":"View More",
            "lock_state_cta":"hmein://activity/DiyFeatures?source=insights_v2",
            "lock_state_cta_text":"UNLOCK SMART NOW",
            "lock_state_subtext":"Get complete access to actionable diet Insights plus a customisable Diet & Workout Plan with HealthifySmart",
            "lock_state_title":"Unlock Recommendations",
            "message":"Insights found",
            "should_show_lock_state":false
        }
    """.trimIndent()

}