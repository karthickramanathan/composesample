package com.sample.compose.riainsight.presentation.ui.insightcards

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sample.compose.R
import com.sample.compose.riainsight.data.model.SummaryCardParameters


@Composable
fun SummaryCard(
    param: SummaryCardParameters,
    modifier: Modifier = Modifier,
) {

    Card(
        modifier = modifier,
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp
    ) {
        Column(modifier = Modifier.padding(16.dp)) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(painter = painterResource(id = R.drawable.img_ria), contentDescription = null)
                Spacer(modifier = Modifier.width(8.dp))
                Text(param.headerText)
            }
            Spacer(modifier = Modifier.padding(16.dp))
            Text(param.bodyText)
        }
    }
}

@Preview
@Composable
private fun SummaryCardPreview() {
    SummaryCard(
        param = SummaryCardParameters(
            headerText = "Hi Karthick  some more text for second line, not enough, adding more",
            bodyText = "Good job! Your stayed in your budget, but you need more protein. I have analysed your calories, macros, and provided suggestions here.",
            summaryText = "Summary"
        )
    )
}