package com.sample.compose.riainsight.presentation.ui.insightcards

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sample.compose.riainsight.data.model.FeedbackCardParameters
import com.sample.compose.theme.colorRiaInsightGreen
import com.sample.compose.theme.colorRiaInsightRed


@Composable
fun FeedbackChoice(
    modifier: Modifier = Modifier,
    question: String = "Was this insight helpful?",
    positiveText: String = "YES",
    negativeText: String = "NO",
    positiveOnClick: () -> Unit = {},
    negativeOnClick: () -> Unit = {},
) {

    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(text = question, fontSize = 12.sp, modifier = Modifier.weight(1f))
        Spacer(modifier = Modifier.width(8.dp))
        Text(
            text = negativeText,
            fontSize = 14.sp,
            modifier = Modifier.clickable(onClick = negativeOnClick)
        )
        Spacer(modifier = Modifier.width(32.dp))
        Text(
            text = positiveText,
            fontSize = 14.sp,
            modifier = Modifier.clickable(onClick = positiveOnClick)
        )
    }
}

@Composable
fun FeedbackCard(
    param: FeedbackCardParameters,
    onFeedbackResponse: (Boolean, String) -> Unit,
    modifier: Modifier = Modifier,
) {

    var selectedChoice by remember { mutableStateOf<Boolean?>(null) }
    var feedbackText by remember { mutableStateOf("") }

    var submitClicked by remember { mutableStateOf(false) }

    if (submitClicked) {
        Card(
            modifier = modifier,
            shape = RoundedCornerShape(8.dp),
            elevation = 4.dp
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            ) {
                Text(text = "Thank you for your response", fontSize = 14.sp)
            }
        }
        return
    }

    Card(
        modifier = modifier,
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp
    ) {
        Column(modifier = Modifier
            .padding(16.dp)
        ) {
            Text(
                text = param.title,
                fontSize = 14.sp,
                color = Color(0xFF717171)
            )
            Spacer(modifier = Modifier.height(16.dp))
            Text(
                text = param.questionText,
                fontSize = 16.sp,
                fontWeight = FontWeight.SemiBold
            )
            Spacer(modifier = Modifier.height(12.dp))
            ChoiceButton(
                modifier = Modifier
                    .fillMaxWidth()
                    .heightIn(min = 48.dp),
                text = "Yes",
                onClick = { selectedChoice = true },
                isSelected = selectedChoice == true
            )
            Spacer(modifier = Modifier.height(8.dp))
            ChoiceButton(
                modifier = Modifier
                    .fillMaxWidth()
                    .heightIn(min = 48.dp),
                text = "No",
                onClick = { selectedChoice = false },
                isSelected = selectedChoice == false
            )
            if (selectedChoice != null) {
                Spacer(modifier = Modifier.height(30.dp))
                Text(
                    text = param.feedbackQuestionText,
                    fontSize = 14.sp,
                    fontWeight = FontWeight.SemiBold
                )
                Spacer(modifier = Modifier.height(14.dp))
                OutlinedTextField(
                    modifier = Modifier
                        .fillMaxWidth(),
                    value = feedbackText,
                    onValueChange = { feedbackText = it },
                    shape = RoundedCornerShape(8.dp),
                    colors = TextFieldDefaults.outlinedTextFieldColors(
                        unfocusedBorderColor = Color(0xFFDFDFDF),
                        focusedBorderColor = colorRiaInsightGreen,
                    ),
                    placeholder = {
                        Text(
                            text = param.feedbackAnswerPlaceholder,
                            color = Color(0xFF9D9D9D),
                            fontSize = 14.sp
                        )
                    }
                )
                Spacer(modifier = Modifier.height(8.dp))
                TextButton(
                    modifier = Modifier.align(Alignment.End),
                    onClick = {
                        onFeedbackResponse(selectedChoice!!, feedbackText)
                        submitClicked = true
                    }
                ) {
                    Text(text = "Submit", fontSize = 14.sp, color = colorRiaInsightRed)
                }
            }
        }
    }
}

@Composable
private fun ChoiceButton(
    text: String,
    isSelected: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {

    val interactionSource = remember { MutableInteractionSource() }
    val isPressed by interactionSource.collectIsPressedAsState()
    val backgroundColor = if (isPressed || isSelected) colorRiaInsightGreen else Color(0xFFF6F6F6)
    val textColor = if (isPressed || isSelected) Color.White else Color.Black

    Button(
        modifier = modifier,
        onClick = onClick,
        interactionSource = interactionSource,
        elevation = ButtonDefaults.elevation(
            defaultElevation = 0.dp,
            pressedElevation = 0.dp
        ),
        shape = RoundedCornerShape(8.dp),
        colors = ButtonDefaults.buttonColors(backgroundColor = backgroundColor)
    ) {
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = text,
            fontSize = 14.sp,
            color = textColor
        )
    }
}

@Preview
@Composable
private fun FeedbackChoicePreview() {
    FeedbackChoice()
}

@Preview
@Composable
private fun FeedbackCardPreview() {
    FeedbackCard(
        param = FeedbackCardParameters(
            feedbackAnswerPlaceholder = "Type your feedback here.",
            feedbackQuestionText = "Anything else you want us to improve?",
            insightsInfo = null,
            questionText = "Karthick, did you find these insights helpful?",
            title = "I'd love some feedback from you..."
        ),
        onFeedbackResponse = { choice, feedback ->

        },
    )
}