package com.sample.compose.riainsight.data.model


import com.google.gson.annotations.SerializedName

data class InsightFeedbackExtraInfo(
    @SerializedName("element_id")
    var elementId: String,
    @SerializedName("insight_card_id")
    var insightCardId: String,
    @SerializedName("is_summary_card")
    var isSummaryCard: Boolean = false,
    @SerializedName("rule_name")
    var ruleName: String?
)