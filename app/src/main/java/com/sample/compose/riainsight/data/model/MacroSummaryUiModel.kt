package com.sample.compose.riainsight.data.model

import androidx.annotation.DrawableRes

class MacroSummaryUiModel(val summaryTitle: String,
                          val summaryDescription: String?,
                          val proteinBalance: MacroInfo,
                          val carbBalance: MacroInfo,
                          val fatsBalance: MacroInfo,
                          val fibreBalance: MacroInfo,
                          val feedbackUI: FeedbackUI? = null)

class MacroInfo(val name: String, val consumedValue: Double, val budgetValue: Double, @DrawableRes val imageRes: Int)