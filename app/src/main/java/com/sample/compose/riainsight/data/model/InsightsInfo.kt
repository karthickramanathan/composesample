package com.sample.compose.riainsight.data.model


import com.google.gson.annotations.SerializedName

class InsightsInfo(
    @SerializedName("extra_info")
    var extraInfo: InsightFeedbackExtraInfo,
    @SerializedName("feedback_key")
    var feedbackKey: String
)