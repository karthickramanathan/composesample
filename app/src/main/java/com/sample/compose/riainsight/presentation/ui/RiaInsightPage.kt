package com.sample.compose.riainsight.presentation.ui

import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBackIos
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.insets.imePadding
import com.sample.compose.base.Result
import com.sample.compose.riainsight.data.model.*
import com.sample.compose.riainsight.presentation.ui.insightcards.*
import com.sample.compose.riainsight.presentation.viewmodel.RiaInsightViewModel
import com.sample.compose.riainsight.presentation.viewmodel.RiaInsightViewModelFactory
import com.sample.compose.riainsight.repository.RiaInsightRepository
import com.sample.compose.riainsight.usecase.RiaInsightData1UseCase
import me.onebone.toolbar.CollapsingToolbarScaffold
import me.onebone.toolbar.ScrollStrategy
import me.onebone.toolbar.rememberCollapsingToolbarScaffoldState


@Composable
fun RiaInsightPage(
    modifier: Modifier = Modifier,
    contentModifier: Modifier = Modifier,
    viewModel: RiaInsightViewModel = viewModel(
        factory = RiaInsightViewModelFactory(
            RiaInsightData1UseCase(RiaInsightRepository())
        )
    ),
    onBackClick: () -> Unit
) {

    val state = rememberCollapsingToolbarScaffoldState()

    //val offsetY = state.offsetY // y offset of the layout
    //val progress = state.toolbarState.progress // how much the toolbar is expanded (0: collapsed, 1: expanded)

    CollapsingToolbarScaffold(
        modifier = modifier
            .fillMaxSize()
            .background(color = Color(0xFFF6F6F6)),
        state = state,
        scrollStrategy = ScrollStrategy.ExitUntilCollapsed,
        toolbar = {

            val textSize = (18 + (30 - 18) * state.toolbarState.progress).sp
            val startPadding = (64 - (48 * state.toolbarState.progress)).dp

            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(120.dp)
                    .pin()
            )

            Icon(
                modifier = Modifier
                    .height(56.dp)
                    .padding(start = 8.dp),
                imageVector = Icons.Rounded.ArrowBackIos,
                contentDescription = null
            )

            Text(
                text = "Your Insights",
                modifier = Modifier
                    .road(Alignment.CenterStart, Alignment.BottomStart)
                    .padding(startPadding, 16.dp, 16.dp),
                color = Color.Black,
                fontSize = textSize
            )


            /* CollapsingToolbar(
                 modifier = Modifier.parallax(ratio = 0.2f),
                 collapsingToolbarState = rememberCollapsingToolbarState()
             ) {
                 Text("Your Insights")
             }*/
        }
    ) {
        val focusManager = LocalFocusManager.current

        Box(
            modifier = Modifier
                .fillMaxSize()
                .clickable(
                    indication = null,
                    interactionSource = remember { MutableInteractionSource() }
                ) {
                    focusManager.clearFocus()
                }
        ) {
            when (val params = viewModel.cardParametersState.value) {
                is Result.Success -> RiaInsightCards(
                    params = params.data,
                    modifier = Modifier
                        .verticalScroll(state = rememberScrollState())
                        .padding(vertical = 16.dp, horizontal = 8.dp)
                )
                is Result.Loading -> CircularProgressIndicator(
                    modifier = Modifier.align(Alignment.Center)
                )
            }
        }
    }
}

@Composable
private fun RiaInsightCards(
    params: List<RiaInsightCardParameters>,
    modifier: Modifier = Modifier,
) {
    Column(modifier = modifier) {
        params.forEachIndexed { index, param ->
            when (param) {
                is SummaryCardParameters -> SummaryCard(param)
                is CalorieCardParameters -> CalorieCard(param)
                is NutrientCardParameters -> NutrientCard(param)
                is ExpandableCardParameters -> ExpandableCard(param, riaCtaOnClick = {})
                is FeedbackCardParameters -> FeedbackCard(
                    param,
                    onFeedbackResponse = { choice, feedback -> })
                is NutritionLevelCardParameters -> NutritionLevelCard(param)
                is RecommendationCardParameters -> RecommendationCard(param, riaCtaOnClick = {})
                is SuggestionCardParameters -> SuggestionCard(param, riaCtaOnClick = {})
            }
            if (index != params.size - 1) {
                Spacer(modifier = Modifier.height(24.dp))
            }
        }
    }
}