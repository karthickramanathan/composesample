package com.sample.compose.riainsight.presentation.viewmodel

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.sample.compose.base.Result
import com.sample.compose.riainsight.data.model.*
import com.sample.compose.riainsight.presentation.ui.RiaInsightType
import com.sample.compose.riainsight.usecase.RiaInsightData1UseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RiaInsightViewModel(
    private val data1UseCase: RiaInsightData1UseCase
) : ViewModel() {

    private val gson = Gson()

    private var riaInsightResponse: RiaInsightResponse? = null

    private val _cardParametersState =
        mutableStateOf<Result<List<RiaInsightCardParameters>>>(Result.Loading)
    val cardParametersState: State<Result<List<RiaInsightCardParameters>>> =
        _cardParametersState

    init {
        getResponse()
    }

    private fun getResponse() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                riaInsightResponse = data1UseCase.invoke()

                delay(2000L)

                riaInsightResponse?.let { response ->
                    val paramList = parseRiaInsightResponse(response)
                    _cardParametersState.value = Result.Success(paramList)
                }
            }
        }
    }

    private fun parseRiaInsightResponse(insightResponse: RiaInsightResponse): List<RiaInsightCardParameters> {
        val paramsList = mutableListOf<RiaInsightCardParameters>()

        for (insight in insightResponse.insights) {
            when (insight.dataDetails.extras.nativeView.viewType) {
                RiaInsightType.VIEW_INSIGHT_SUMMARY_CARD -> {
                    paramsList.add(
                        getParsedCardItem(insight, SummaryCardParameters::class.java)
                    )
                }
                RiaInsightType.VIEW_MACRO_CONSUMED -> {
                    paramsList.add(
                        getParsedCardItem(insight, CalorieCardParameters::class.java)
                    )
                }
                RiaInsightType.VIEW_PFCF_SUMMARY_CARD -> {
                    paramsList.add(
                        getParsedCardItem(insight, NutrientCardParameters::class.java)
                    )
                }
                RiaInsightType.VIEW_ALTERNATIVE_EXPAND_CARD -> {
                    paramsList.add(
                        getParsedCardItem(insight, ExpandableCardParameters::class.java)
                    )
                }
                RiaInsightType.VIEW_FEEDBACK_CARD -> {
                    paramsList.add(
                        getParsedCardItem(insight, FeedbackCardParameters::class.java)
                    )
                }
                RiaInsightType.VIEW_NUTRITION_LEVEL_INSIGHT -> {
                    paramsList.add(
                        getParsedCardItem(insight, NutritionLevelCardParameters::class.java)
                    )
                }
                RiaInsightType.VIEW_MEAL_PORTIONS_CARD -> {
                    paramsList.add(
                        getParsedCardItem(insight, RecommendationCardParameters::class.java)
                    )
                }
                RiaInsightType.VIEW_MEAL_SUGGESTION_CARD -> {
                    paramsList.add(
                        getParsedCardItem(insight, SuggestionCardParameters::class.java)
                    )
                }
            }
        }

        return paramsList
    }

    private fun <T> getParsedCardItem(insight: Insight, classOfT: Class<T>): T {
        return gson.fromJson(insight.dataDetails.extras.nativeView.parameters, classOfT)
    }
}

class RiaInsightViewModelFactory(
    private val data1UseCase: RiaInsightData1UseCase
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RiaInsightViewModel(data1UseCase) as T
    }
}