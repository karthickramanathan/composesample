package com.sample.compose.riainsight.data.model

class InsightLockConfig(
    var lockStateTitle: String? = null,
    var lockStateSubtext: String? = null,
    var lockStateCtaText: String? = null,
    var lockStateCta: String? = null
)