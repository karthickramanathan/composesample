package com.sample.compose.riainsight.presentation.ui.insightcards

import android.content.Context
import android.graphics.Typeface
import androidx.annotation.VisibleForTesting
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.sample.compose.R
import com.sample.compose.base.utils.parseARGBComposeColor
import com.sample.compose.riainsight.data.model.NutritionInfo
import com.sample.compose.riainsight.data.model.NutritionInfoMap
import com.sample.compose.riainsight.data.model.NutritionLevelCardParameters
import com.sample.compose.riainsight.data.model.PfcRatio
import com.sample.compose.theme.*
import android.graphics.Color as AndroidColor


@Composable
fun NutritionLevelCard(
    param: NutritionLevelCardParameters,
    modifier: Modifier = Modifier,
) {
    Card(
        modifier = modifier,
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp
    ) {
        Column {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .heightIn(min = 64.dp)
                    .background(
                        color = param.headerBackground.parseARGBComposeColor(colorRiaInsightLightOrange)
                    )
                    .padding(16.dp),
            ) {
                Text(
                    modifier = Modifier.align(Alignment.CenterStart),
                    text = param.headerText,
                    fontSize = 16.sp,
                )
            }
            Spacer(modifier = Modifier.height(8.dp))
            if (param.summary != null)
                Text(
                    modifier = Modifier
                        .heightIn(min = 56.dp)
                        .padding(vertical = 8.dp, horizontal = 16.dp),
                    text = param.summary,
                    fontSize = 14.sp
                )
            Row(modifier = Modifier.padding(horizontal = 16.dp)) {
                NutritionLevelChart(
                    modifier = Modifier
                        .weight(1f)
                        .aspectRatio(1f),
                    pfcRatio = param.goalPfcRatio,
                )
                Spacer(modifier = Modifier.width(8.dp))
                NutritionLevelChart(
                    modifier = Modifier
                        .weight(1f)
                        .aspectRatio(1f),
                    pfcRatio = param.consumedPfcRatio,
                )
            }
            Row(modifier = Modifier.padding(horizontal = 16.dp)) {
                Text(
                    modifier = Modifier
                        .weight(1f)
                        .padding(8.dp),
                    text = "Goal PFC Ratio",
                    textAlign = TextAlign.Center,
                    fontSize = 12.sp,
                )
                Text(
                    modifier = Modifier
                        .weight(1f)
                        .padding(8.dp),
                    text = "Consumed PFC Ratio",
                    textAlign = TextAlign.Center,
                    fontSize = 12.sp,
                )
            }
            NutritionLevelSummaryItem(
                color = colorRiaInsightCarb,
                name = "Carbs",
                info = param.nutritionInfoMap.carbs,
                modifier = Modifier.padding(16.dp)
            )
            NutritionLevelSummaryItem(
                color = colorRiaInsightFat,
                name = "Fats",
                info = param.nutritionInfoMap.fats,
                modifier = Modifier.padding(16.dp)
            )
            NutritionLevelSummaryItem(
                color = colorRiaInsightProtein,
                name = "Proteins",
                info = param.nutritionInfoMap.protein,
                modifier = Modifier.padding(16.dp)
            )
            Spacer(modifier = Modifier.height(8.dp))
            Divider()
            FeedbackChoice(
                modifier = modifier
                    .heightIn(min = 56.dp)
                    .padding(16.dp),
            )
        }
    }
}

private const val CHART_LABEL_TEXT_SIZE_IN_SP: Float = 12f

@Composable
private fun NutritionLevelChart(
    pfcRatio: PfcRatio,
    modifier: Modifier = Modifier
) {
    Box(modifier = modifier.fillMaxSize()) {
        Image(
            modifier = Modifier.fillMaxSize(),
            painter = painterResource(id = R.drawable.chart_plate),
            contentDescription = null
        )
        AndroidView(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            factory = { context ->
                PieChart(context).apply {
                    isDrawHoleEnabled = false
                    description.isEnabled = false
                    isRotationEnabled = false
                    rotationAngle = 90f
                    legend.isEnabled = false
                    setEntryLabelColor(AndroidColor.WHITE)
                    setEntryLabelTypeface(Typeface.DEFAULT_BOLD)
                    setEntryLabelTextSize(CHART_LABEL_TEXT_SIZE_IN_SP)
                    data = getPfcChartPieData(context, pfcRatio)
                }
            })
    }
}

@VisibleForTesting
const val STATUS_LOW: Int = 0

@VisibleForTesting
const val STATUS_BALANCED: Int = 1

@VisibleForTesting
const val STATUS_HIGH: Int = 2

@Composable
private fun NutritionLevelSummaryItem(
    color: Color,
    name: String,
    info: NutritionInfo,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier,
    ) {
        Box(
            modifier = Modifier
                .size(16.dp)
                .background(color = color, shape = RoundedCornerShape(4.dp))
        )
        Spacer(modifier = Modifier.width(16.dp))
        Text(text = name, fontSize = 14.sp, modifier = Modifier.weight(1f))
        Spacer(modifier = Modifier.width(16.dp))
        Text(
            text = getNutrientLevelBalance(info),
            fontSize = 14.sp,
            textAlign = TextAlign.End,
            modifier = Modifier.weight(1f)
        )
        Spacer(modifier = Modifier.width(16.dp))
        Text(
            text = getNutrientLevelStatus(info.status),
            fontSize = 12.sp,
            textAlign = TextAlign.End,
            color = getNutrientLevelStatusColor(info.status),
            modifier = Modifier.weight(1f)
        )
    }

}

private fun getPfcChartPieData(context: Context, pfcRatio: PfcRatio): PieData {
    val xValues = arrayListOf<PieEntry>()
    xValues.add(
        PieEntry(pfcRatio.carbs.toFloat(), getPfcChartLabel(pfcRatio.carbs))
    )
    xValues.add(
        PieEntry(pfcRatio.fats.toFloat(), getPfcChartLabel(pfcRatio.fats))
    )
    xValues.add(
        PieEntry(pfcRatio.protein.toFloat(), getPfcChartLabel(pfcRatio.protein))
    )

    val pieDataSet = PieDataSet(xValues, null)

    pieDataSet.setColors(
        ContextCompat.getColor(context, R.color.ria_insight_carb_color),
        ContextCompat.getColor(context, R.color.ria_insight_fat_color),
        ContextCompat.getColor(context, R.color.ria_insight_protein_color)
    )
    pieDataSet.sliceSpace = 8f
    pieDataSet.setDrawValues(false)

    return PieData(pieDataSet)
}

/**
 * Show label only if it can fit in to the chart slice
 * @return value with percentage or empty string if value is less for text fit into slice
 */
@VisibleForTesting
fun getPfcChartLabel(value: Int): String = if (value > 17) "$value%" else ""

@VisibleForTesting
@Composable
fun getNutrientLevelBalance(nutritionInfo: NutritionInfo): String {
    return stringResource(
        R.string.nutrition_level_budget_template,
        nutritionInfo.consumed,
        nutritionInfo.budget
    )
}

@VisibleForTesting
fun getNutrientLevelStatus(status: Int): String {
    return when (status) {
        STATUS_LOW -> "Low"
        STATUS_BALANCED -> "Balanced"
        STATUS_HIGH -> "High"
        else -> "Low"
    }
}

@VisibleForTesting
fun getNutrientLevelStatusColor(status: Int): Color = when (status) {
    STATUS_LOW -> colorRiaInsightYellow
    STATUS_BALANCED -> colorRiaInsightGreen
    STATUS_HIGH -> colorRiaInsightRed
    else -> colorRiaInsightYellow
}

////////////////////////////////////////////////////////////////////////////////////////////////////

@Preview
@Composable
private fun NutritionLevelCardPreview() {
    NutritionLevelCard(
        param = NutritionLevelCardParameters(
            headerBackground = "#FFF1ES",
            headerText = "Dinner Macronutrients Ratio",
            summary = "Seems like your Dinner was high in carb. Let's take a look at your macronutrient breakdown.",
            insightsInfo = null,
            nutritionInfoMap = NutritionInfoMap(
                carbs = NutritionInfo(
                    budget = 40.97541646957397,
                    consumed = 58.49040091728404,
                    status = 2
                ),
                fats = NutritionInfo(
                    budget = 10.926777725219726,
                    consumed = 2.680304057367092,
                    status = 0
                ),
                protein = NutritionInfo(
                    budget = 16.39016658782959,
                    consumed = 11.27627781311223,
                    status = 1
                ),
            ),
            consumedPfcRatio = PfcRatio(
                carbs = 80,
                fats = 4,
                protein = 16
            ),
            goalPfcRatio = PfcRatio(
                carbs = 50,
                fats = 30,
                protein = 20
            ),
        )
    )
}