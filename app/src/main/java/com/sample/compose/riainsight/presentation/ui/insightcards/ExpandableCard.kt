package com.sample.compose.riainsight.presentation.ui.insightcards

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.KeyboardArrowDown
import androidx.compose.material.icons.rounded.KeyboardArrowUp
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sample.compose.base.utils.parseARGBComposeColor
import com.sample.compose.riainsight.data.model.AlternativeFood
import com.sample.compose.riainsight.data.model.ExpandableCardParameters
import com.sample.compose.riainsight.data.model.Food
import com.sample.compose.riainsight.presentation.ui.components.RiaCtaButton
import com.sample.compose.theme.colorRiaInsightGreen
import com.sample.compose.theme.colorRiaInsightLightOrange


@Composable
fun ExpandableCard(
    param: ExpandableCardParameters,
    riaCtaOnClick: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    Card(
        modifier = modifier,
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp
    ) {
        Column {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .heightIn(min = 64.dp)
                    .background(
                        color = param.headerBackground.parseARGBComposeColor(
                            colorRiaInsightLightOrange
                        )
                    )
                    .padding(16.dp),
            ) {
                Text(
                    modifier = Modifier.align(Alignment.CenterStart),
                    text = param.headerText,
                    fontSize = 16.sp,
                )
            }
            param.foods.forEachIndexed { index, food ->
                ExpandableItem(
                    food = food,
                    isExpandedByDefault = index == 0,
                    modifier = Modifier.padding(horizontal = 16.dp)
                )
                if (index != param.foods.size - 1)
                    Spacer(modifier = Modifier.height(8.dp))
            }
            Spacer(modifier = Modifier.height(8.dp))
            RiaCtaButton(
                modifier = Modifier
                    .height(48.dp),
                text = param.riaCtaText,
                onClick = { riaCtaOnClick(param.riaCta) }
            )
            Spacer(modifier = Modifier.height(8.dp))
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun ExpandableItem(
    food: Food,
    modifier: Modifier = Modifier,
    isExpandedByDefault: Boolean = false,
) {
    var isExpanded by remember { mutableStateOf(isExpandedByDefault) }

    Column(modifier = modifier) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Column(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxWidth()
            ) {
                Spacer(modifier = Modifier.height(8.dp))
                Text(text = food.name, fontSize = 14.sp)
                Spacer(modifier = Modifier.height(8.dp))
                Text(text = getFoodHeaderSubtitle(food), fontSize = 12.sp)
            }
            IconButton(onClick = { isExpanded = !isExpanded }) {
                Icon(
                    imageVector = if (isExpanded) Icons.Rounded.KeyboardArrowUp
                    else Icons.Rounded.KeyboardArrowDown,
                    contentDescription = null,
                )
            }
        }
        AnimatedVisibility(visible = isExpanded) {
            ExpandItemChild(food = food)
        }
    }
}

@Composable
private fun ExpandItemChild(food: Food, modifier: Modifier = Modifier) {
    Column(modifier = modifier) {
        if (!food.summary.isNullOrBlank()) {
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = food.summary,
                fontSize = 12.sp,
                color = Color.Gray
            )
        }
        if (!food.alternatives.isNullOrEmpty()) {
            val sectionTitle =
                if (food.alternativeSectionTitle.isNullOrBlank()) "Healthier alternatives for this food"
                else food.alternativeSectionTitle
            Box(
                modifier = Modifier.heightIn(min = 48.dp),
                contentAlignment = Alignment.CenterStart
            ) {
                Text(text = sectionTitle, fontSize = 14.sp)
            }
            food.alternatives.forEachIndexed { index, alternativeFood ->
                ExpandFoodItem(
                    food = alternativeFood,
                    modifier = Modifier.heightIn(min = 72.dp)
                )
                if (index == food.alternatives.size - 1)
                    Divider()
            }
        }
    }
}

@Composable
private fun ExpandFoodItem(
    food: AlternativeFood,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            modifier = Modifier
                .size(48.dp)
                .clip(CircleShape)
                .background(color = colorRiaInsightGreen, shape = CircleShape)
                .padding(8.dp),
            text = food.name[0].uppercase(),
            fontSize = 18.sp,
            color = Color.White,
            textAlign = TextAlign.Center,
        )
        Spacer(modifier = Modifier.width(16.dp))
        Column(
            modifier = Modifier
                .weight(1f)
                .wrapContentHeight()
        ) {
            Text(text = food.name, fontSize = 14.sp)
            Spacer(modifier = Modifier.height(8.dp))
            Text(text = getFoodQuantityWithInfo(food), fontSize = 14.sp)
        }
        if (!food.calories.isNullOrEmpty())
            Text(
                text = "${food.calories} Cal",
                fontSize = 12.sp
            )
    }
}

private fun getFoodHeaderSubtitle(food: Food): AnnotatedString {
    return buildAnnotatedString {
        append(food.qty)
        val calories = food.calories?.toString()
        if (!calories.isNullOrBlank()) {
            append(" • ")
            append("$calories Cal")
        }
        if (!food.macroInfo.isNullOrBlank()) {
            append(" • ")
            withStyle(
                style = SpanStyle(
                    color = food.macroInfoColor.parseARGBComposeColor(colorRiaInsightGreen)
                )
            ) {
                append(food.macroInfo)
            }
        }
    }
}

private fun getFoodQuantityWithInfo(food: AlternativeFood): AnnotatedString {
    return buildAnnotatedString {
        append(food.qty)
        if (!food.macroInfo.isNullOrBlank()) {
            append(" ")
            append("•")
            append(" ")
            withStyle(style = SpanStyle(color = colorRiaInsightGreen)) {
                append(food.macroInfo)
            }
        }
    }
}

@Preview
@Composable
private fun ExpandableCardPreview() {
    ExpandableCard(
        riaCtaOnClick = {},
        param = ExpandableCardParameters(
            feedbackKey = "1-cid:58-eid:993",
            foods = listOf(
                Food(
                    calories = 326,
                    foodId = 188,
                    macroInfo = "36gm Protein",
                    macroInfoColor = "#8BC249",
                    name = "Curry Chicken",
                    qty = "2.00 katori",
                    summary = "Your protein intake is impressive. Here's the food you tracked. Keep up the good work!",
                    alternatives = listOf(
                        AlternativeFood(
                            calories = "82",
                            foodId = 188,
                            macroInfo = "4gm Fat",
                            macroInfoColor = "#8BC24",
                            name = "Curry Chicken",
                            qty = "0.50 katori",
                            foodImage = null
                        )
                    ),
                    alternativeSectionTitle = "Recommended portions",
                    measure = ""
                ),
                Food(
                    calories = 171,
                    foodId = 22,
                    macroInfo = "6gm Protein",
                    macroInfoColor = "#8BC249",
                    name = "Sapathi",
                    qty = "2.00 roti/chapati",
                    summary = "Your protein intake is impressive. Here's the food you tracked. Keep up the good work!",
                    alternatives = null,
                    alternativeSectionTitle = null,
                    measure = ""
                ),
            ),
            headerText = "High protein Foods had during Lunch",
            headerBackground = "#EDF7E1",
            riaCta = "",
            riaCtaText = "GET MORE SUGGESTIONS FROM RIA",
        )
    )
}