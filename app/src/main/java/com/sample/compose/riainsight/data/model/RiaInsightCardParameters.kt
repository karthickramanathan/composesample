package com.sample.compose.riainsight.data.model


import com.google.gson.annotations.SerializedName

sealed class RiaInsightCardParameters

class SummaryCardParameters(
    @SerializedName("icon_url")
    var iconUrl: String? = null,
    @SerializedName("header_text")
    var headerText: String,
    @SerializedName("body_text")
    var bodyText: String,
    @SerializedName("actions")
    var actions: List<SummaryCardAction>? = null,
    @SerializedName("summary_text")
    var summaryText: String,
    @SerializedName(KEY_INSIGHTS_INFO)
    var insightsInfo: InsightsInfo? = null
) : RiaInsightCardParameters()

class CalorieCardParameters(
    @SerializedName("header_text")
    var headerText: String,
    @SerializedName("macro_consumed")
    var macroConsumed: String,
    @SerializedName("macro_budget")
    var macroBudget: String,
    @SerializedName("macro_fill_colour")
    var macroFillColour: String?,
    @SerializedName("summary")
    var summary: String,
    @SerializedName("macro_info")
    var macroInfo: String,
    @SerializedName(KEY_INSIGHTS_INFO)
    var insightsInfo: InsightsInfo?
) : RiaInsightCardParameters()

class NutrientCardParameters(
    @SerializedName("calorie_consumed")
    var calorieConsumed: Double,
    @SerializedName("calorie_budget")
    var macroBudget: Double,
    @SerializedName("summary")
    var summary: String,

    @SerializedName("carb")
    var carb: Double,
    @SerializedName("carb_budget")
    var carbBudget: Double,

    @SerializedName("protein")
    var protein: Double,
    @SerializedName("protein_budget")
    var proteinBudget: Double,

    @SerializedName("fat")
    var fat: Double,
    @SerializedName("fat_budget")
    var fatBudget: Double,

    @SerializedName("fibre")
    var fibre: Double,
    @SerializedName("fibre_budget")
    var fibreBudget: Double,

    @SerializedName("header_text")
    var headerText: String,

    @SerializedName("header_background_color")
    var headerBackground: String,

    @SerializedName("feedback_key")
    var feedbackKey: String?,

    @SerializedName(KEY_INSIGHTS_INFO)
    var insightsInfo: InsightsInfo?
) : RiaInsightCardParameters()

class SummaryCardAction(
    @SerializedName("action_cta")
    val actionCta: String?,
    @SerializedName("action_text")
    val actionText: String,
    @SerializedName("is_primary")
    val isPrimary: Boolean
)