package com.sample.compose.riainsight.presentation.ui.insightcards

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sample.compose.R
import com.sample.compose.base.utils.parseARGBComposeColor
import com.sample.compose.riainsight.data.model.NutrientCardParameters
import com.sample.compose.theme.colorRiaInsightGreen
import com.sample.compose.theme.colorRiaInsightLightOrange
import com.sample.compose.theme.colorRiaInsightRed
import com.sample.compose.theme.colorRiaInsightYellow
import kotlin.math.roundToInt

@Composable
fun NutrientCard(
    param: NutrientCardParameters,
    modifier: Modifier = Modifier,
) {
    Card(
        modifier = modifier,
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp
    ) {
        Column {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .heightIn(min = 64.dp)
                    .background(
                        color = param.headerBackground.parseARGBComposeColor(
                            colorRiaInsightLightOrange
                        )
                    )
                    .padding(16.dp),
            ) {
                Text(
                    modifier = Modifier.align(Alignment.CenterStart),
                    text = param.headerText,
                    fontSize = 16.sp,
                )
            }
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                modifier = modifier
                    .padding(16.dp),
                text = param.summary,
                fontSize = 14.sp
            )
            Spacer(modifier = Modifier.height(8.dp))
            NutrientSummaryItem(
                modifier = modifier
                    .heightIn(min = 48.dp)
                    .padding(16.dp),
                nutrientIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_ria_insight_protein),
                        contentDescription = null
                    )
                },
                nutrientName = "Protein",
                nutrientBudget = param.proteinBudget,
                nutrientConsumed = param.protein,
            )
            NutrientSummaryItem(
                modifier = modifier
                    .heightIn(min = 48.dp)
                    .padding(16.dp),
                nutrientIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_ria_insight_fats),
                        contentDescription = null
                    )
                },
                nutrientName = "Fats",
                nutrientBudget = param.fatBudget,
                nutrientConsumed = param.fat,
            )
            NutrientSummaryItem(
                modifier = modifier
                    .heightIn(min = 48.dp)
                    .padding(16.dp),
                nutrientIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_ria_insight_carb),
                        contentDescription = null
                    )
                },
                nutrientName = "Carbs",
                nutrientBudget = param.carbBudget,
                nutrientConsumed = param.carb,
            )
            NutrientSummaryItem(
                modifier = modifier
                    .heightIn(min = 48.dp)
                    .padding(16.dp),
                nutrientIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_ria_insight_fibre),
                        contentDescription = null
                    )
                },
                nutrientName = "Fibre",
                nutrientBudget = param.fibreBudget,
                nutrientConsumed = param.fibre,
            )
            Spacer(modifier = Modifier.height(8.dp))
            Divider()
            FeedbackChoice(
                modifier = modifier
                    .heightIn(min = 56.dp)
                    .padding(16.dp),
            )
        }
    }
}

@Composable
private fun NutrientSummaryItem(
    nutrientIcon: @Composable () -> Unit,
    nutrientName: String,
    nutrientBudget: Double,
    nutrientConsumed: Double,
    modifier: Modifier = Modifier,
) {
    val macroColor = getMacroProgressColor(nutrientName, nutrientConsumed, nutrientBudget)

    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        nutrientIcon()
        Spacer(modifier = Modifier.width(4.dp))
        Text(
            text = nutrientName,
            fontSize = 12.sp,
            modifier = Modifier.weight(1f)
        )
        Spacer(modifier = Modifier.width(4.dp))
        Text(
            modifier = Modifier.weight(2f),
            text = getBalanceString(nutrientConsumed, nutrientBudget),
            textAlign = TextAlign.End,
            fontSize = 12.sp,
        )
        Spacer(modifier = Modifier.width(8.dp))

        val percentage = getPercentageCompletion(nutrientConsumed, nutrientBudget)
        val progress: Float = if (percentage > 100) 1f else percentage / 100f

        LinearProgressIndicator(
            modifier = Modifier.weight(2f),
            progress = progress,
            color = macroColor
        )
        Spacer(modifier = Modifier.width(8.dp))
        Text(
            text = getPercentageString(nutrientConsumed, nutrientBudget),
            color = macroColor,
            modifier = Modifier.weight(0.8f),
            textAlign = TextAlign.End,
            fontSize = 12.sp,
        )
    }
}

private fun getPercentageCompletion(consumedValue: Double, budgetValue: Double): Int {
    return (consumedValue / budgetValue * 100).roundToInt()
}

private fun getPercentageString(consumedValue: Double, budgetValue: Double): String {
    return "${getPercentageCompletion(consumedValue, budgetValue)}%"
}

@Composable
private fun getBalanceString(consumedValue: Double, budgetValue: Double): String {
    return stringResource(R.string.macro_budget_template, consumedValue, budgetValue)
}

private fun getMacroProgressColor(
    macroName: String, consumedValue: Double, budgetValue: Double
): Color {
    val percentageCompletion = getPercentageCompletion(consumedValue, budgetValue)
    return if (macroName == "Protein" || macroName == "Fibre") {
        when (percentageCompletion) {
            in 0..70 -> colorRiaInsightYellow
            in 71..120 -> colorRiaInsightGreen
            else -> colorRiaInsightRed
        }
    } else {
        when (percentageCompletion) {
            in 0..50 -> colorRiaInsightYellow
            in 51..100 -> colorRiaInsightGreen
            else -> colorRiaInsightRed
        }
    }
}

@Preview
@Composable
private fun NutrientCardPreview() {
    NutrientCard(
        param = NutrientCardParameters(
            summary = "Your breakfast was low in protein and high in carbs - not ideal for losing that weight.",
            calorieConsumed = 171.0,
            macroBudget = 328.0,
            carb = 57.9,
            carbBudget = 256.0,
            protein = 25.9,
            proteinBudget = 20.5,
            fat = 47.9,
            fatBudget = 68.3,
            fibre = 30.5,
            fibreBudget = 30.5,
            headerText = "Breakfast Macronutrients breakup",
            headerBackground = "#FFF1E5",
            feedbackKey = null,
            insightsInfo = null
        )
    )
}