package com.sample.compose.riainsight.data.model


import com.google.gson.annotations.SerializedName

data class ExpandableCardParameters(
  @SerializedName("feedback_key")
  var feedbackKey: String,
  @SerializedName("foods")
  var foods: List<Food>,
  @SerializedName("header_background_color")
  var headerBackground: String?,
  @SerializedName("header_text")
  var headerText: String,
  @SerializedName("ria_cta")
  var riaCta: String,
  @SerializedName("ria_cta_text")
  var riaCtaText: String
) : RiaInsightCardParameters()