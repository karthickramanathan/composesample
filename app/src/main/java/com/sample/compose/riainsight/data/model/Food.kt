package com.sample.compose.riainsight.data.model


import com.google.gson.annotations.SerializedName

data class Food(
  @SerializedName("alternatives")
  val alternatives: List<AlternativeFood>?,
  @SerializedName("calories")
  var calories: Int?,
  @SerializedName("macro_info")
  var macroInfo: String,
  @SerializedName("macro_info_color")
  var macroInfoColor: String,
  @SerializedName("measure")
  var measure: String,
  @SerializedName("name")
  var name: String,
  @SerializedName("qty")
  var qty: String,
  @SerializedName("summary")
  val summary: String?,
  @SerializedName("food_id")
  var foodId: Long,
  @SerializedName("alternatives_section_title")
  val alternativeSectionTitle:String?
)