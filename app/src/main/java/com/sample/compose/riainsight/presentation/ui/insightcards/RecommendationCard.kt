package com.sample.compose.riainsight.presentation.ui.insightcards

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sample.compose.base.utils.parseARGBComposeColor
import com.sample.compose.riainsight.data.model.RecommendationCardParameters
import com.sample.compose.riainsight.data.model.RecommendedFood
import com.sample.compose.riainsight.presentation.ui.components.RiaCtaButton
import com.sample.compose.theme.colorRiaInsightGreen


@Composable
fun RecommendationCard(
    param: RecommendationCardParameters,
    riaCtaOnClick: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    Card(
        modifier = modifier,
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp
    ) {
        Column {
            Column(modifier = Modifier.padding(horizontal = 16.dp)) {
                Row(
                    modifier = Modifier.heightIn(min = 56.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = param.headerText,
                        fontSize = 16.sp,
                        fontWeight = FontWeight.SemiBold,
                        modifier = Modifier.weight(1f)
                    )
                    Text(
                        text = "${param.calorieBudget} Cal",
                        fontSize = 16.sp,
                        fontWeight = FontWeight.SemiBold,
                    )
                }
                Text(
                    text = param.summaryText,
                    fontSize = 12.sp,
                    modifier = Modifier.padding(vertical = 12.dp)
                )
                for (food in param.foods)
                    RecommendFoodItem(
                        modifier = Modifier.heightIn(min = 88.dp),
                        food = food
                    )
                Spacer(modifier = Modifier.height(8.dp))
            }
            RiaCtaButton(
                modifier = Modifier.height(48.dp),
                text = param.riaCtaText,
                onClick = { riaCtaOnClick(param.riaCta) }
            )
            Spacer(modifier = Modifier.height(8.dp))
        }
    }
}

@Composable
private fun RecommendFoodItem(
    food: RecommendedFood,
    modifier: Modifier = Modifier,
) {
    val foodInfoColor = food.foodInfoColour.parseARGBComposeColor(colorRiaInsightGreen)
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Column(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth()
        ) {
            Text(text = food.name, fontSize = 14.sp)
            if (!food.recommendedQtyInfo.isNullOrEmpty()) {
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    text = food.name,
                    fontSize = 12.sp,
                    color = foodInfoColor
                )
            }
            if (!food.quantity.isNullOrEmpty()) {
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    text = food.quantity,
                    fontSize = 12.sp,
                    color = Color(0xFF717171)
                )
            }
            if (!food.positiveFeedback.isNullOrEmpty()) {
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    text = food.positiveFeedback,
                    fontSize = 12.sp,
                    color = Color(0xFF717171)
                )
            }
        }
        Text(
            text = "${food.calories} Cal",
            fontSize = 14.sp,
            color = if (food.recommendedQtyInfo.isNullOrEmpty()) Color.Black else foodInfoColor
        )
    }
}

@Preview
@Composable
private fun RecommendationCardPreview() {
    RecommendationCard(
        riaCtaOnClick = {},
        param = RecommendationCardParameters(
            calorieBudget = 353,
            headerText = "Revised Lunch Portions",
            summaryText = "I like the Lunch that you ate. But including excess of Plain Cooked Rice made it calorie-heavy. I just played around with the portions to make it healthier for your goal. Give it a try!",
            riaCtaText = "GET MORE SUGGESTIONS FROM RIA",
            riaCta = "",
            insightsInfo = null,
            foods = listOf(
                RecommendedFood(
                    calories = 116,
                    foodId = 32,
                    foodInfoColour = "#8BC249",
                    name = "Red Gram dal",
                    positiveFeedback = "This was perfect, good job!",
                    quantity = "You had - 1.0 katori <b>·</b> 116 cal",
                    recommendedQtyInfo = ""
                ),
                RecommendedFood(
                    calories = 24,
                    foodId = 10406,
                    foodInfoColour = "#8BC249",
                    name = "Sprouts chaat",
                    positiveFeedback = "",
                    quantity = "You had - 1.5 katori <b>·</b> 35 cal",
                    recommendedQtyInfo = "Recommended - 1.0 katori"
                ),
                RecommendedFood(
                    calories = 68,
                    foodId = 9161,
                    foodInfoColour = "#8BC249",
                    name = "Bhindi Bhaji",
                    positiveFeedback = "This was perfect, good job!",
                    quantity = "You had - 1.0 katori <b>·</b> 68 cal",
                    recommendedQtyInfo = ""
                ),
                RecommendedFood(
                    calories = 85,
                    foodId = 22,
                    foodInfoColour = "#8BC249",
                    name = "Roti",
                    positiveFeedback = "This was perfect, good job!",
                    quantity = "You had - 1.0 roti/chapati <b>·</b> 85 cal",
                    recommendedQtyInfo = ""
                ),
                RecommendedFood(
                    calories = 60,
                    foodId = 29,
                    foodInfoColour = "#8BC249",
                    name = "Plain Cooked Rice",
                    positiveFeedback = "",
                    quantity = "",
                    recommendedQtyInfo = "Recommended - 0.5 katori"
                ),
            )
        )
    )
}