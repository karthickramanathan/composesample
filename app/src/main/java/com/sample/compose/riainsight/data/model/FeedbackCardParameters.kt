package com.sample.compose.riainsight.data.model


import com.google.gson.annotations.SerializedName

data class FeedbackCardParameters(
  @SerializedName("feedback_answer_placeholder")
  var feedbackAnswerPlaceholder: String,
  @SerializedName("feedback_question_text")
  var feedbackQuestionText: String,
  @SerializedName("insights_info")
  var insightsInfo: InsightsInfo?,
  @SerializedName("question_text")
  var questionText: String,
  @SerializedName("title")
  var title: String
) : RiaInsightCardParameters()