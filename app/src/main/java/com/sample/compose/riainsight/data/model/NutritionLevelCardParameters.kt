package com.sample.compose.riainsight.data.model

import com.google.gson.annotations.SerializedName

data class NutritionLevelCardParameters(
    @SerializedName("consumed_pfc_ratio")
    val consumedPfcRatio: PfcRatio,
    @SerializedName("goal_pfc_ratio")
    val goalPfcRatio: PfcRatio,
    @SerializedName("header_background_color")
    val headerBackground: String,
    @SerializedName("header_text")
    val headerText: String,
    @SerializedName("insights_info")
    val insightsInfo: InsightsInfo?,
    @SerializedName("nutrition_info_map")
    val nutritionInfoMap: NutritionInfoMap,
    @SerializedName("summary")
    val summary: String?
) : RiaInsightCardParameters()

data class PfcRatio(
    @SerializedName("carbs")
    val carbs: Int,
    @SerializedName("fats")
    val fats: Int,
    @SerializedName("protein")
    val protein: Int
)

data class NutritionInfoMap(
    @SerializedName("carbs")
    val carbs: NutritionInfo,
    @SerializedName("fats")
    val fats: NutritionInfo,
    @SerializedName("protein")
    val protein: NutritionInfo
)

data class NutritionInfo(
    @SerializedName("budget")
    val budget: Double,
    @SerializedName("consumed")
    val consumed: Double,
    @SerializedName("status")
    val status: Int
)