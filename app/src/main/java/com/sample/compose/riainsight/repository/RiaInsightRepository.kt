package com.sample.compose.riainsight.repository

import com.google.gson.Gson
import com.sample.compose.riainsight.data.RiaInsightData
import com.sample.compose.riainsight.data.model.RiaInsightResponse

class RiaInsightRepository {

    suspend fun getData1(): RiaInsightResponse {
        val response = Gson().fromJson(RiaInsightData.data1, RiaInsightResponse::class.java)
        return response
    }
}