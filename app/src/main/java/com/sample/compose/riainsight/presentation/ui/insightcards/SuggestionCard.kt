package com.sample.compose.riainsight.presentation.ui.insightcards

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sample.compose.R
import com.sample.compose.riainsight.data.model.RecommendedFood
import com.sample.compose.riainsight.data.model.SuggestionCardParameters
import com.sample.compose.riainsight.presentation.ui.components.RiaCtaButton
import com.sample.compose.theme.colorRiaInsightGreen


@Composable
fun SuggestionCard(
    param: SuggestionCardParameters,
    riaCtaOnClick: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    Card(
        modifier = modifier,
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp
    ) {
        Column {
            Column(modifier = Modifier.padding(horizontal = 16.dp)) {
                Row(
                    modifier = Modifier.heightIn(min = 56.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = param.headerText,
                        fontSize = 16.sp,
                        fontWeight = FontWeight.SemiBold,
                        modifier = Modifier.weight(1f)
                    )
                    Text(
                        text = "${param.calorieBudget} Cal",
                        fontSize = 16.sp,
                        fontWeight = FontWeight.SemiBold,
                    )
                }
                Text(
                    text = param.summaryText,
                    fontSize = 12.sp,
                    modifier = Modifier.padding(vertical = 12.dp)
                )
                for (food in param.foods)
                    SuggestionFoodItem(
                        modifier = Modifier.heightIn(min = 88.dp),
                        food = food
                    )
                Spacer(modifier = Modifier.height(8.dp))
            }
            RiaCtaButton(
                modifier = Modifier.height(48.dp),
                text = param.riaCtaText,
                onClick = { riaCtaOnClick(param.riaCta) }
            )
            Spacer(modifier = Modifier.height(8.dp))
        }
    }
}

@Composable
private fun SuggestionFoodItem(
    food: RecommendedFood,
    modifier: Modifier = Modifier,
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            modifier = Modifier
                .size(48.dp)
                .background(color = colorRiaInsightGreen, shape = CircleShape)
                .clip(shape = CircleShape),
            painter = painterResource(id = R.drawable.ic_insight_food),
            contentDescription = null
        )
        Spacer(modifier = Modifier.width(16.dp))
        Column(
            verticalArrangement = Arrangement.Center,
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth()
        ) {
            Text(text = food.name, fontSize = 14.sp)
            if (!food.quantity.isNullOrEmpty()) {
                Spacer(modifier = Modifier.height(4.dp))
                Text(text = food.quantity, fontSize = 12.sp)
            }
        }
        Spacer(modifier = Modifier.width(16.dp))
        Text(
            text = "${food.calories} Cal",
            fontSize = 14.sp
        )
    }
}

@Preview
@Composable
private fun SuggestionCardPreview() {
    SuggestionCard(
        riaCtaOnClick = {},
        param = SuggestionCardParameters(
            calorieBudget = 328,
            headerText = "Dinner Suggestion",
            summaryText = "Good choice of a meal! Here is a similar meal suggestion for you.",
            riaCtaText = "GET MORE SUGGESTIONS FROM RIA",
            riaCta = "",
            insightsInfo = null,
            foods = listOf(
                RecommendedFood(
                    calories = 171,
                    foodId = 22,
                    foodInfoColour = "",
                    name = "Roti",
                    positiveFeedback = "",
                    quantity = "2.0 roti/chapati",
                    recommendedQtyInfo = ""
                ),
                RecommendedFood(
                    calories = 83,
                    foodId = 27798,
                    foodInfoColour = "",
                    name = "Broccoli Capsicum Paneer Subzi",
                    positiveFeedback = "",
                    quantity = "0.5 katori",
                    recommendedQtyInfo = ""
                )
            )
        )
    )
}