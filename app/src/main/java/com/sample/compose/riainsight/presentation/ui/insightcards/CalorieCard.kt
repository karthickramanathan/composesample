package com.sample.compose.riainsight.presentation.ui.insightcards

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.datasource.CollectionPreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.sample.compose.R
import com.sample.compose.components.circleprogress.CircleProgress
import com.sample.compose.riainsight.data.model.CalorieCardParameters
import com.sample.compose.theme.colorGrey
import com.sample.compose.theme.colorRiaInsightGreen
import com.sample.compose.theme.colorRiaInsightRed
import com.sample.compose.theme.colorRiaInsightYellow
import kotlin.math.roundToInt

@Composable
fun CalorieCard(
    param: CalorieCardParameters,
    modifier: Modifier = Modifier,
) {
    Card(
        modifier = modifier,
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp
    ) {
        Column(modifier = Modifier.padding(16.dp)) {
            Row(
                modifier = Modifier.heightIn(min = 48.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = param.headerText,
                    fontWeight = FontWeight.SemiBold,
                    modifier = Modifier.weight(1f)
                )
                Text(
                    text = stringResource(
                        id = R.string.insight_cal_budget_template,
                        param.macroConsumed,
                        param.macroBudget
                    ),
                    fontWeight = FontWeight.SemiBold,
                )
            }
            Text(
                text = param.summary,
                modifier = Modifier.padding(vertical = 8.dp),
            )

            val budgetInt = try {
                param.macroBudget.toFloat()
            } catch (e: Exception) {
                0f
            }
            val consumedInt = try {
                param.macroConsumed.toFloat()
            } catch (e: Exception) {
                0f
            }

            val budgetCompletion: Float = if (budgetInt > 0) consumedInt / budgetInt * 100 else 0f

            CalorieInsightView(
                budgetCompletion = budgetCompletion,
                descText = param.macroInfo,
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
            )
        }
    }
}

@Composable
fun CalorieInsightView(
    budgetCompletion: Float,
    descText: String,
    modifier: Modifier = Modifier
) {

    val mainColor = when (budgetCompletion) {
        in 0.0..80.0 -> colorRiaInsightYellow
        in 80.01..110.0 -> colorRiaInsightGreen
        else -> colorRiaInsightRed
    }

    val valueUnder1Float = if (budgetCompletion > 100f) 0f
    else (100f - budgetCompletion) / 100

    ConstraintLayout(modifier = modifier) {

        val (progressBox, progressLine, descTextView, caloriePercentageTextView) = createRefs()

        Box(
            modifier = Modifier
                .size(64.dp)
                .constrainAs(progressBox) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    bottom.linkTo(parent.bottom)
                }
        ) {
            CircleProgress(
                progress = if (budgetCompletion > 100) 100 else budgetCompletion.roundToInt(),
                finishedColor = mainColor,
                unFinishedColor = colorGrey,
                modifier = Modifier.size(64.dp)
            )
            Image(
                modifier = Modifier
                    .size(40.dp)
                    .align(Alignment.Center),
                painter = painterResource(id = R.drawable.ic_insight_food),
                contentDescription = null
            )
        }
        Box(modifier = Modifier
            .height(1.dp)
            .background(color = mainColor)
            .constrainAs(progressLine) {
                linkTo(
                    top = progressBox.top,
                    bottom = progressBox.bottom,
                    start = progressBox.start,
                    end = caloriePercentageTextView.start,
                    startMargin = 32.dp,
                    endMargin = 12.dp,
                    verticalBias = valueUnder1Float
                )
                width = Dimension.fillToConstraints
            }
        )
        Text(
            modifier = Modifier.constrainAs(descTextView) {
                linkTo(
                    start = progressBox.end,
                    end = caloriePercentageTextView.start,
                    startMargin = 12.dp,
                    endMargin = 12.dp,
                )
                if (valueUnder1Float > 0.5f) bottom.linkTo(progressLine.top, margin = 8.dp)
                else top.linkTo(progressLine.bottom, margin = 8.dp)
                width = Dimension.fillToConstraints
            },
            text = descText,
            fontSize = 12.sp,
        )
        Text(
            text = "${budgetCompletion.roundToInt()}%",
            color = mainColor,
            fontSize = 32.sp,
            modifier = Modifier.constrainAs(caloriePercentageTextView) {
                linkTo(
                    top = progressLine.top,
                    bottom = progressLine.top,
                )
                end.linkTo(parent.end)
            }
        )
    }
}

@Preview
@Composable
private fun CalorieCardPreview() {
    CalorieCard(
        param = CalorieCardParameters(
            headerText = "Today’s Calories",
            macroConsumed = "303",
            macroBudget = "328",
            macroFillColour = "#8BC249",
            summary = "You did well. It’s good to maintain a 10-15% deficit when trying to lose weight.",
            macroInfo = "You have a 15% calorie deficit today",
            insightsInfo = null
        )
    )
}


class CalorieInsightProvider : CollectionPreviewParameterProvider<Float>(
    listOf(0f, 40f, 80f, 81f, 110f, 111f)
)

@Preview
@Composable
private fun CalorieInsightViewPreview(
    @PreviewParameter(CalorieInsightProvider::class) budgetCompletion: Float
) {
    CalorieInsightView(
        budgetCompletion = budgetCompletion,
        descText = "You have a 15% calorie deficit today",
        modifier = Modifier.size(300.dp, 100.dp)
    )
}