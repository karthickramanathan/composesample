package com.sample.compose.riainsight.presentation.ui

object RiaInsightType {
    const val VIEW_INSIGHT_SUMMARY_CARD = 18
    const val VIEW_MACRO_CONSUMED = 19
    const val VIEW_NUTRITION_LEVEL_INSIGHT = 20
    const val VIEW_PFCF_SUMMARY_CARD = 0
    const val VIEW_ALTERNATIVE_EXPAND_CARD = 21
    const val VIEW_FEEDBACK_CARD = 22
    const val VIEW_MEAL_PORTIONS_CARD = 23
    const val VIEW_MEAL_SUGGESTION_CARD = 24
}