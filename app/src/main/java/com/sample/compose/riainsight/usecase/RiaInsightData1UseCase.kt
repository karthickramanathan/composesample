package com.sample.compose.riainsight.usecase

import com.sample.compose.riainsight.data.model.RiaInsightResponse
import com.sample.compose.riainsight.repository.RiaInsightRepository

class RiaInsightData1UseCase(private val repository: RiaInsightRepository) {

    suspend operator fun invoke(): RiaInsightResponse {
        return repository.getData1()
    }
}