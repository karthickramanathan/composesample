package com.sample.compose.riainsight.data.model


import com.google.gson.annotations.SerializedName

data class AlternativeFood(
  @SerializedName("food_image")
  var foodImage: String?,
  @SerializedName("macro_info")
  var macroInfo: String,
  @SerializedName("macro_info_color")
  var macroInfoColor: String,
  @SerializedName("name")
  var name: String,
  @SerializedName("qty")
  var qty: String,
  @SerializedName("calories")
  var calories: String?,
  @SerializedName("id")
  var foodId: Long
)