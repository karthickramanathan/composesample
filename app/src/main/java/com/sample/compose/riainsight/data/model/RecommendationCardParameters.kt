package com.sample.compose.riainsight.data.model

import com.google.gson.annotations.SerializedName


data class RecommendationCardParameters(
    @SerializedName("calorie_budget")
    val calorieBudget: Int,
    @SerializedName("foods")
    val foods: List<RecommendedFood>,
    @SerializedName("header_text")
    val headerText: String,
    @SerializedName("ria_cta")
    val riaCta: String,
    @SerializedName("insights_info")
    val insightsInfo: InsightsInfo?,
    @SerializedName("ria_cta_text")
    val riaCtaText: String,
    @SerializedName("summary_text")
    val summaryText: String
) : RiaInsightCardParameters()

data class SuggestionCardParameters(
    @SerializedName("calorie_budget")
    val calorieBudget: Int,
    @SerializedName("foods")
    val foods: List<RecommendedFood>,
    @SerializedName("header_text")
    val headerText: String,
    @SerializedName("ria_cta")
    val riaCta: String,
    @SerializedName("insights_info")
    val insightsInfo: InsightsInfo?,
    @SerializedName("ria_cta_text")
    val riaCtaText: String,
    @SerializedName("summary_text")
    val summaryText: String
) : RiaInsightCardParameters()

data class RecommendedFood(
    @SerializedName("calories")
    val calories: Int,
    @SerializedName("id")
    val foodId: Long,
    @SerializedName("food_info_colour")
    val foodInfoColour: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("positive_feedback")
    val positiveFeedback: String?,
    @SerializedName("qty")
    val quantity: String?,
    @SerializedName("recommended_qty_info")
    val recommendedQtyInfo: String?
)