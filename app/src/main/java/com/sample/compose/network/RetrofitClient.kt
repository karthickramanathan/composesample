package com.sample.compose.network

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClient {

    private const val BASE_URL_V1 = "https://api.healthifyme.com/api/v1/"
    private const val BASE_URL_V2 = "https://api.healthifyme.com/api/v2/"

    private val authorization = "ApiKey +918680815691:a7c9f62386ba8a41e38ad84af3b1579ba70214bb"

    private fun getHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().apply {
            addInterceptor(
                Interceptor {chain ->
                    val originalRequest = chain.request()
                    val originalUrl = originalRequest.url()

                    val url = originalUrl.newBuilder()
                        .addQueryParameter("auth_user_id", "19782590")
                        .build()

                    val requestBuilder = originalRequest.newBuilder().url(url)

                    requestBuilder.header("authorization", authorization)
                    return@Interceptor chain.proceed(requestBuilder.build())
                }
            )
        }.build()
    }

    fun getRetrofitV1(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL_V1)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(getHttpClient())
            .build()
    }

    fun getRetrofitV2(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL_V2)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

}