package com.sample.compose.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.sample.compose.dietplan.presentation.ui.DiyDietPlanPage
import com.sample.compose.main.MainPage
import com.sample.compose.riainsight.presentation.ui.RiaInsightPage
import com.sample.compose.skubackpress.presentation.ui.SkuBackPress
import com.sample.compose.stories.presentation.ui.StoriesPage

object NavDestinations {
    const val MAIN = "main"
    const val DIY_DIET_PLAN_EDIT_PREFERENCE = "diy_diet_plan_edit_preference"
    const val SKU_BACK_PRESS = "sku_back_press"
    const val RIA_INSIGHT = "ria_insight"
    const val STORIES = "stories"

    val allDestinations = listOf(
        DIY_DIET_PLAN_EDIT_PREFERENCE,
        SKU_BACK_PRESS,
        RIA_INSIGHT,
        STORIES
        )
}

@Composable
fun SampleNavGraph(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
    startDestination: String = NavDestinations.MAIN,
) {

    NavHost(
        navController = navController,
        startDestination = startDestination
    ) {

        composable(NavDestinations.MAIN) {
            MainPage { navController.navigate(it) }
        }
        composable(NavDestinations.DIY_DIET_PLAN_EDIT_PREFERENCE) {
            DiyDietPlanPage { navController.popBackStack() }
        }
        composable(NavDestinations.SKU_BACK_PRESS) {
            SkuBackPress { navController.popBackStack() }
        }
        composable(NavDestinations.RIA_INSIGHT) {
            RiaInsightPage { navController.popBackStack() }
        }
        composable(NavDestinations.STORIES) {
            StoriesPage { navController.popBackStack() }
        }
    }
}