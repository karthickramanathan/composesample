package com.sample.compose.dietplan.data.model


import com.google.gson.annotations.SerializedName

data class DiyEditPrefResponse(
    @SerializedName("cta_card_config_list")
    var ctaCardConfigList: List<CtaCardConfig>,
    @SerializedName("edit_preference_groups")
    var editPreferenceGroups: List<EditPreferenceGroup>
)