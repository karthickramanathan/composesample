package com.sample.compose.dietplan.data.api

import com.sample.compose.dietplan.data.model.DiyEditPrefResponse
import com.sample.compose.network.RetrofitClient
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface DiySwitcherApiService {

    @GET("diy/user_preferences/edit_preferences/config")
    fun getEditPreferenceConfig(): Single<Response<DiyEditPrefResponse>>
}

object DiySwitcherApi {

    private val apiService: DiySwitcherApiService by lazy {
        RetrofitClient.getRetrofitV1().create(DiySwitcherApiService::class.java)
    }

    fun getEditPreferenceConfig():Single<Response<DiyEditPrefResponse>> {
        return apiService.getEditPreferenceConfig()
    }
}