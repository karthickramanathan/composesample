package com.sample.compose.dietplan.data.model


import com.google.gson.annotations.SerializedName

data class CtaCardConfig(
  @SerializedName("background_color")
  var backgroundColor: String,
  @SerializedName("card_bg_url")
  var cardBgUrl: String,
  @SerializedName("card_image_url")
  var cardImageUrl: String,
  @SerializedName("cta_color")
  var ctaColor: String,
  @SerializedName("cta_text")
  var ctaText: String,
  @SerializedName("cta_url")
  var ctaUrl: String,
  @SerializedName("text_color")
  var textColor: String,
  @SerializedName("title")
  var title: String,
  @SerializedName("description")
  var description: String?
)