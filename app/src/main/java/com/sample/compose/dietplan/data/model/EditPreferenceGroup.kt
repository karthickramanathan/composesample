package com.sample.compose.dietplan.data.model


import com.google.gson.annotations.SerializedName

data class EditPreferenceGroup(
  @SerializedName("edit_preference_options")
  var editPreferenceOptions: List<EditPreferenceOption>,
  @SerializedName("group_header")
  var groupHeader: String
)