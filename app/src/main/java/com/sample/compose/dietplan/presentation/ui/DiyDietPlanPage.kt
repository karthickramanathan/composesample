package com.sample.compose.dietplan.presentation.ui

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForwardIos
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.compositeOver
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.sample.compose.COMPOSE_TAG
import com.sample.compose.R
import com.sample.compose.base.Result
import com.sample.compose.components.ScrollToTopButton
import com.sample.compose.dietplan.data.model.CtaCardConfig
import com.sample.compose.dietplan.data.model.DiyEditPrefResponse
import com.sample.compose.dietplan.data.model.EditPreferenceGroup
import com.sample.compose.dietplan.data.model.EditPreferenceOption
import com.sample.compose.dietplan.domain.DietPlanEditConfigUseCase
import com.sample.compose.dietplan.presentation.viewmodel.DietPlanViewModel
import com.sample.compose.dietplan.presentation.viewmodel.DietPlanViewModelFactory
import com.sample.compose.dietplan.repository.DietPlanRepository
import kotlinx.coroutines.launch


@Composable
fun DiyDietPlanPage(
    modifier: Modifier = Modifier,
    contentModifier: Modifier = Modifier,
    viewModel: DietPlanViewModel = viewModel(
        factory = DietPlanViewModelFactory(
            DietPlanEditConfigUseCase(DietPlanRepository())
        )
    ),
    onBackClick: () -> Unit,
) {

    Log.d(COMPOSE_TAG, "DiyDietPlanPage")

    val bodyLazyColumnState = rememberLazyListState()

    Scaffold(
        modifier = modifier.fillMaxSize(),
        topBar = {
            //Log.d(COMPOSE_TAG, "TopAppBar")

            val topBarElevation by remember {
                derivedStateOf {
                    /*Log.d(
                        COMPOSE_TAG,
                        "Scroll Offset : " + bodyLazyColumnState.firstVisibleItemScrollOffset
                    )*/
                    if (bodyLazyColumnState.firstVisibleItemScrollOffset > 10) 8.dp
                    else 0.dp
                }
            }

            TopAppBar(
                title = { Text("Edit Preference") },
                elevation = topBarElevation,
                backgroundColor = Color.White,
                navigationIcon = {
                    IconButton(onClick = onBackClick) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = null
                        )
                    }
                },
            )
        }
    ) { paddingValues ->

        Log.d(COMPOSE_TAG, "Scaffold Body")

        Surface(modifier = contentModifier.fillMaxSize()) {
            Box {
                when (val config = viewModel.getDiyEditPrefConfigState().value) {
                    is Result.Loading -> CircularProgressIndicator(
                        modifier = Modifier.align(Alignment.Center)
                    )
                    is Result.Error -> Text(
                        modifier = Modifier.align(Alignment.Center),
                        text = config.exception?.message ?: config.message ?: "Error",
                    )
                    is Result.Success -> {
                        DietPlanBody(
                            config = config.data,
                            onCardClick = {},
                            onOptionClick = {},
                            lazyColumnState = bodyLazyColumnState
                        )
                    }
                }
            }
        }
    }
}

@Composable
private fun BoxScope.DietPlanBody(
    config: DiyEditPrefResponse,
    onCardClick: (CtaCardConfig) -> Unit,
    onOptionClick: (EditPreferenceOption) -> Unit,
    modifier: Modifier = Modifier,
    lazyColumnState: LazyListState = rememberLazyListState(),
) {
    val coroutineScope = rememberCoroutineScope()

    LazyColumn(
        modifier = modifier,
        state = lazyColumnState,
        contentPadding = PaddingValues(top = 16.dp, bottom = 72.dp)
    ) {
        Log.d(COMPOSE_TAG, "LazyColumn Body")
        items(config.ctaCardConfigList.size) { index ->
            CtaCard(
                modifier = Modifier.padding(horizontal = 12.dp),
                ctaConfig = config.ctaCardConfigList[index],
                onClick = onCardClick
            )
        }
        item {
            Spacer(
                modifier = Modifier
                    .height(8.dp)
                    .background(color = Color.LightGray)
            )
        }
        items(config.editPreferenceGroups.size) { index ->
            EditPreferenceGroupView(
                group = config.editPreferenceGroups[index],
                onOptionClick = onOptionClick
            )
        }
    }

    val jumpThreshold = with(LocalDensity.current) {
        56.dp.toPx()
    }

    val showScrollToTopButton = remember {
        derivedStateOf {
            lazyColumnState.firstVisibleItemIndex != 0 ||
                    lazyColumnState.firstVisibleItemScrollOffset > jumpThreshold
        }
    }
    ScrollToTopButton(
        enabled = showScrollToTopButton.value,
        modifier = Modifier.align(Alignment.BottomCenter),
        onClick = {
            coroutineScope.launch {
                lazyColumnState.animateScrollToItem(0, 0)
            }
        }
    )
}

@Composable
private fun CtaCard(
    modifier: Modifier = Modifier,
    ctaConfig: CtaCardConfig,
    onClick: (CtaCardConfig) -> Unit,
) {

    Log.d(COMPOSE_TAG, "CtaCard Body")

    Card(
        modifier = modifier
            .clickable(onClick = { onClick(ctaConfig) }),
        elevation = 2.dp,
        backgroundColor = Color.LightGray.copy(alpha = 0.4F).compositeOver(Color.White),
    ) {
        /* Image(
             painter = rememberGlidePainter(request = ctaConfig.cardBgUrl),
             contentScale = ContentScale.Crop,
             contentDescription = null
         )*/
        Row(
            modifier = Modifier
                .height(IntrinsicSize.Min)
        ) {
            Column(
                modifier = Modifier
                    .weight(0.7F)
                    .padding(16.dp),
                verticalArrangement = Arrangement.Center
            ) {
                Text(text = ctaConfig.title)
                if (ctaConfig.description != null) {
                    Spacer(modifier = Modifier.height(2.dp))
                    Text(text = ctaConfig.description!!)
                }
                Spacer(modifier = Modifier.height(8.dp))
                Text(ctaConfig.ctaText)
            }
            Image(
                modifier = Modifier
                    .weight(0.3F),
                painter = rememberImagePainter(
                    data = ctaConfig.cardImageUrl,
                    builder = {
                        placeholder(R.drawable.hme)
                    }
                ),
                contentScale = ContentScale.Crop,
                contentDescription = null
            )
        }
    }
}

@Composable
private fun EditPreferenceGroupView(
    modifier: Modifier = Modifier,
    group: EditPreferenceGroup,
    onOptionClick: (EditPreferenceOption) -> Unit,
) {

    //Log.d(COMPOSE_TAG, "EditPreferenceGroupView Body")

    Column(modifier = modifier.wrapContentHeight()) {
        Text(
            modifier = Modifier.padding(12.dp),
            text = group.groupHeader,
            fontWeight = FontWeight.SemiBold
        )
        group.editPreferenceOptions.forEach { option ->
            EditPreferenceOptionView(
                option = option,
                onClick = onOptionClick
            )
        }
    }
}

@Composable
private fun EditPreferenceOptionView(
    modifier: Modifier = Modifier,
    option: EditPreferenceOption,
    onClick: (EditPreferenceOption) -> Unit,
) {
    Row(
        modifier = modifier
            .clickable { onClick(option) }
            .fillMaxWidth()
            .padding(horizontal = 12.dp, vertical = 12.dp),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Image(
            modifier = Modifier.size(32.dp),
            painter = rememberImagePainter(
                data = option.iconUrl,
                builder = {
                    placeholder(R.drawable.hme)
                }
            ),
            contentDescription = null,
        )
        Column(
            Modifier
                .weight(1F)
                .padding(horizontal = 12.dp),
        ) {
            Text(
                text = option.title,
                fontSize = 14.sp,
                maxLines = 1,
                overflow = TextOverflow.Clip,
            )
            if (!option.subText.isNullOrEmpty()) {
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    text = option.subText,
                    fontSize = 12.sp,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
            }
        }
        Icon(
            modifier = Modifier.size(20.dp),
            imageVector = Icons.Default.ArrowForwardIos,
            contentDescription = null,
            tint = Color.DarkGray
        )
    }
}

@Preview
@Composable
fun CtaCardPreview() {
    val ctaCardConfig = CtaCardConfig(
        backgroundColor = "#ffffff",
        cardBgUrl = "",
        cardImageUrl = "",
        ctaColor = "#356CC4",
        ctaText = "GET IMMUNITY DIET",
        ctaUrl = "hmein://activity/SPPlanSwitcherDetail?id=2&show_cta=true",
        textColor = "#000000",
        title = "Its time to start building your immunity",
        description = null
    )
    CtaCard(ctaConfig = ctaCardConfig, onClick = {})
}

@Preview
@Composable
fun EditPreferenceOptionViewPreview() {
    EditPreferenceOptionView(
        option = EditPreferenceOption(
            ctaUrl = "",
            iconUrl = "",
            title = "Title",
            subText = "Sub Text"
        ),
        onClick = {}
    )
}
