package com.sample.compose.dietplan.repository

import com.sample.compose.dietplan.data.api.DiySwitcherApi
import com.sample.compose.dietplan.data.model.DiyEditPrefResponse
import io.reactivex.Single
import retrofit2.Response

class DietPlanRepository {


    fun getDiyEditPrefConfig(): Single<Response<DiyEditPrefResponse>> {
        return DiySwitcherApi.getEditPreferenceConfig()
    }
}