package com.sample.compose.dietplan.data.model


import com.google.gson.annotations.SerializedName

data class EditPreferenceOption(
  @SerializedName("cta_url")
  var ctaUrl: String,
  @SerializedName("icon_url")
  var iconUrl: String,
  @SerializedName("sub_text")
  var subText: String,
  @SerializedName("title")
  var title: String
)