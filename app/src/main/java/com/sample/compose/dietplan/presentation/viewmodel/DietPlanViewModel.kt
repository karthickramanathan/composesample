package com.sample.compose.dietplan.presentation.viewmodel

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sample.compose.base.Result
import com.sample.compose.base.applyIOSchedulersSingle
import com.sample.compose.dietplan.data.model.DiyEditPrefResponse
import com.sample.compose.dietplan.domain.DietPlanEditConfigUseCase
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import retrofit2.Response

class DietPlanViewModel(
    private val dietPlanEditConfigUseCase: DietPlanEditConfigUseCase
) : ViewModel() {

    private val disposableList = mutableListOf<Disposable>()

    init {
        getDiyEditPrefConfig()
    }

    private val diyEditPrefConfigState = mutableStateOf<Result<DiyEditPrefResponse>>(Result.Loading)


    fun getDiyEditPrefConfigState(): State<Result<DiyEditPrefResponse>> = diyEditPrefConfigState

    private fun getDiyEditPrefConfig() {
        dietPlanEditConfigUseCase.getDiyEditPrefConfig()
            .applyIOSchedulersSingle()
            .subscribe(object : SingleObserver<Response<DiyEditPrefResponse>> {
                override fun onSubscribe(d: Disposable) {
                    disposableList.add(d)
                }

                override fun onSuccess(t: Response<DiyEditPrefResponse>) {
                    val body = t.body()
                    diyEditPrefConfigState.value =
                        if (t.isSuccessful && body != null) Result.Success(body)
                        else Result.Error(message = "Error")
                }

                override fun onError(e: Throwable) {
                    diyEditPrefConfigState.value = Result.Error(exception = Exception(e))
                }

            })

    }

    override fun onCleared() {
        super.onCleared()
        disposableList.forEach { it.dispose() }
    }
}

class DietPlanViewModelFactory(
    private val dietPlanEditConfigUseCase: DietPlanEditConfigUseCase
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DietPlanViewModel(dietPlanEditConfigUseCase) as T
    }
}