package com.sample.compose.dietplan.domain

import com.sample.compose.base.Result
import com.sample.compose.dietplan.data.model.DiyEditPrefResponse
import com.sample.compose.dietplan.repository.DietPlanRepository
import io.reactivex.Single
import retrofit2.Response

class DietPlanEditConfigUseCase(private val repository: DietPlanRepository) {

    fun getDiyEditPrefConfig() : Single<Response<DiyEditPrefResponse>> {
        return repository.getDiyEditPrefConfig()
    }
}