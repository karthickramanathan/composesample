package com.sample.compose.components.countdowntimer

import android.os.CountDownTimer
import android.util.Log
import androidx.compose.runtime.*
import kotlinx.coroutines.delay


@Composable
fun rememberCountDownTimer(
    startDelayInMillis: Long,
    maxTimeInMillis: Long,
    tickTimeInMillis: Long = 1000L,
    onTimerFinished: (() -> Unit)? = null
): State<Long> {

    val remainingTime =
        remember(startDelayInMillis, maxTimeInMillis) { mutableStateOf(maxTimeInMillis) }

    val timer: CountDownTimer = remember(startDelayInMillis, maxTimeInMillis) {
        object : CountDownTimer(maxTimeInMillis, tickTimeInMillis) {
            override fun onTick(millisUntilFinished: Long) {
                remainingTime.value = millisUntilFinished
            }

            override fun onFinish() {
                onTimerFinished?.invoke()
            }
        }
    }

    LaunchedEffect(startDelayInMillis, maxTimeInMillis) {
        delay(startDelayInMillis)
        timer.cancel()
        timer.start()
    }

    DisposableEffect(startDelayInMillis, maxTimeInMillis) {
        onDispose { timer.cancel() }
    }

    return remainingTime
}

class CountDownTimerState(
    private val maxTimeInMillis: Long,
    private val tickTimeInMillis: Long = 1000L,
) {

    var onTimerFinished: (() -> Unit)? = null

    val remainingTime = mutableStateOf(maxTimeInMillis)

    private val timer: CountDownTimer = object : CountDownTimer(maxTimeInMillis, tickTimeInMillis) {
        override fun onTick(millisUntilFinished: Long) {
            remainingTime.value = millisUntilFinished
            Log.d("some", millisUntilFinished.toString())
        }

        override fun onFinish() {
            onTimerFinished?.invoke()
        }
    }

    fun startTimer() {
        timer.start()
    }

    fun cancelTimer() {
        timer.cancel()
    }

    fun onDispose() {
        timer.cancel()
    }
}