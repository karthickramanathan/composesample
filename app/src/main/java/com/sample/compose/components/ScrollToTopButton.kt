package com.sample.compose.components

import android.annotation.SuppressLint
import androidx.compose.animation.core.Transition
import androidx.compose.animation.core.animateDp
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.material.ExtendedFloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowUpward
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

private enum class ButtonVisibility {
    VISIBLE,
    GONE
}

/**
 * Shows a button that lets the user scroll to the top.
 */
@Composable
fun ScrollToTopButton(
    enabled: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    // Show Scroll to Top button
    val transition: Transition<ButtonVisibility> = updateTransition(
        targetState = if (enabled) ButtonVisibility.VISIBLE else ButtonVisibility.GONE,
        label = "ScrollToTopTransition"
    )

    val bottomOffset by transition.animateDp(
        label = "ScrollToTopAnimateDp"
    ) {
        if (it == ButtonVisibility.GONE) (-32).dp else 32.dp
    }

    //Log.d(COMPOSE_TAG, "ScrollToTopButton : bottomOffset : $bottomOffset")

    if (bottomOffset > 0.dp) {
        ExtendedFloatingActionButton(
            icon = {
                Icon(
                    imageVector = Icons.Filled.ArrowUpward,
                    modifier = Modifier.height(18.dp),
                    contentDescription = null
                )
            },
            text = {
                Text(text = "Scroll To Top")
            },
            onClick = onClick,
            backgroundColor = MaterialTheme.colors.surface,
            contentColor = MaterialTheme.colors.primary,
            modifier = modifier
                .offset(x = 0.dp, y = -bottomOffset)
                .height(36.dp)
        )
    }
}

@SuppressLint("UnrememberedMutableState")
@Preview
@Composable
private fun ScrollToTopButtonPreview() {
    ScrollToTopButton(enabled = true, onClick = {})
}
