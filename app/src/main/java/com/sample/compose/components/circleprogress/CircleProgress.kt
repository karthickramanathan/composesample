package com.sample.compose.components.circleprogress


import androidx.annotation.IntRange
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sample.compose.theme.colorGrey
import com.sample.compose.theme.colorRiaInsightGreen
import kotlin.math.acos


@Composable
fun CircleProgress(
    @IntRange(from = 0, to = 100) progress: Int,
    finishedColor: Color,
    unFinishedColor: Color,
    modifier: Modifier = Modifier
) {
    Canvas(modifier = modifier) {
        val progressHeight = progress / 100f * size.height
        val radius = size.width / 2
        val angle =
            (acos(((radius - progressHeight) / radius).toDouble()) * 180 / Math.PI).toFloat()

        val startAngle = 90 + angle
        val sweepAngle = 360 - angle * 2

        drawArc(
            color = unFinishedColor,
            startAngle = startAngle,
            sweepAngle = sweepAngle,
            useCenter = false
        )

        rotate(
            degrees = 180F,
            pivot = Offset(size.width / 2, size.height / 2)
        ) {
            drawArc(
                color = finishedColor,
                startAngle = 270 - angle,
                sweepAngle = angle * 2,
                useCenter = false
            )
        }
    }
}

@Preview
@Composable
private fun CircleProgressPreview() {
    CircleProgress(
        progress = 70,
        finishedColor = colorRiaInsightGreen,
        unFinishedColor = colorGrey,
        modifier = Modifier.size(100.dp)
    )
}