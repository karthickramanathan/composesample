package com.sample.compose.components.coupon

import androidx.compose.foundation.shape.CornerBasedShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.ui.geometry.*
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.addOutline
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp


class RoundedCornerCouponShape(
    topStart: CornerSize,
    topEnd: CornerSize,
    bottomEnd: CornerSize,
    bottomStart: CornerSize,
    private val middleStartRadius: Dp,
    private val middleEndRadius: Dp,
) : CornerBasedShape(
    topStart = topStart,
    topEnd = topEnd,
    bottomEnd = bottomEnd,
    bottomStart = bottomStart,
) {

    override fun createOutline(
        size: Size,
        topStart: Float,
        topEnd: Float,
        bottomEnd: Float,
        bottomStart: Float,
        layoutDirection: LayoutDirection
    ): Outline {
        val cornerOutline: Outline = if (topStart + topEnd + bottomEnd + bottomStart == 0.0f) {
            Outline.Rectangle(size.toRect())
        } else {
            Outline.Rounded(
                RoundRect(
                    rect = size.toRect(),
                    topLeft = CornerRadius(if (layoutDirection == LayoutDirection.Ltr) topStart else topEnd),
                    topRight = CornerRadius(if (layoutDirection == LayoutDirection.Ltr) topEnd else topStart),
                    bottomRight = CornerRadius(if (layoutDirection == LayoutDirection.Ltr) bottomEnd else bottomStart),
                    bottomLeft = CornerRadius(if (layoutDirection == LayoutDirection.Ltr) bottomStart else bottomEnd)
                )
            )
        }

        return Outline.Generic(
            path = RoundedCornerCouponPath(
                size = size,
                cornerOutline = cornerOutline,
                middleStartRadius = middleStartRadius,
                middleEndRadius = middleEndRadius,
                layoutDirection = layoutDirection,
            )
        )
    }

    override fun copy(
        topStart: CornerSize,
        topEnd: CornerSize,
        bottomEnd: CornerSize,
        bottomStart: CornerSize,
    ) = RoundedCornerCouponShape(
        topStart = topStart,
        topEnd = topEnd,
        bottomEnd = bottomEnd,
        bottomStart = bottomStart,
        middleStartRadius = middleStartRadius,
        middleEndRadius = middleEndRadius,
    )

}

private fun RoundedCornerCouponPath(
    size: Size,
    cornerOutline: Outline,
    middleStartRadius: Dp,
    middleEndRadius: Dp,
    layoutDirection: LayoutDirection,
) = Path().apply {

    reset()
    addOutline(cornerOutline)

    val leftMiddleRadius =
        if (layoutDirection == LayoutDirection.Ltr) middleStartRadius else middleEndRadius
    val rightMiddleRadius =
        if (layoutDirection == LayoutDirection.Ltr) middleEndRadius else middleStartRadius

    addArc(
        oval = Rect(
            center = Offset(x = 0f, y = size.height / 2),
            radius = leftMiddleRadius.value
        ),
        startAngleDegrees = -90f,
        sweepAngleDegrees = 180f
    )

    addArc(
        oval = Rect(
            center = Offset(x = size.width, y = size.height / 2),
            radius = rightMiddleRadius.value
        ),
        startAngleDegrees = 90f,
        sweepAngleDegrees = 180f
    )

}

fun RoundedCornerCouponShape(corner: CornerSize, middleStartRadius: Dp, middleEndRadius: Dp) =
    RoundedCornerCouponShape(corner, corner, corner, corner, middleStartRadius, middleEndRadius)

fun RoundedCornerCouponShape(size: Dp, middleRadius: Dp) =
    RoundedCornerCouponShape(CornerSize(size), middleRadius, middleRadius)

fun RoundedCornerCouponShape(size: Float, middleRadius: Float) =
    RoundedCornerCouponShape(CornerSize(size), middleRadius.dp, middleRadius.dp)

fun RoundedCornerCouponShape(
    topStart: Dp = 0.dp,
    topEnd: Dp = 0.dp,
    bottomEnd: Dp = 0.dp,
    bottomStart: Dp = 0.dp,
    middleRadius: Dp = 0.dp
) = RoundedCornerCouponShape(
    topStart = CornerSize(topStart),
    topEnd = CornerSize(topEnd),
    bottomEnd = CornerSize(bottomEnd),
    bottomStart = CornerSize(bottomStart),
    middleStartRadius = middleRadius,
    middleEndRadius = middleRadius,
)

fun RoundedCornerCouponShape(
    topStart: Dp = 0.dp,
    topEnd: Dp = 0.dp,
    bottomEnd: Dp = 0.dp,
    bottomStart: Dp = 0.dp,
    middleStartRadius: Dp = 0.dp,
    middleEndRadius: Dp = 0.dp
) = RoundedCornerCouponShape(
    topStart = CornerSize(topStart),
    topEnd = CornerSize(topEnd),
    bottomEnd = CornerSize(bottomEnd),
    bottomStart = CornerSize(bottomStart),
    middleStartRadius = middleStartRadius,
    middleEndRadius = middleEndRadius,
)

fun RoundedCornerCouponShape(
    topStart: Float = 0.0f,
    topEnd: Float = 0.0f,
    bottomEnd: Float = 0.0f,
    bottomStart: Float = 0.0f,
    middleRadius: Float = 0.0f
) = RoundedCornerCouponShape(
    topStart = CornerSize(topStart),
    topEnd = CornerSize(topEnd),
    bottomEnd = CornerSize(bottomEnd),
    bottomStart = CornerSize(bottomStart),
    middleStartRadius = middleRadius.dp,
    middleEndRadius = middleRadius.dp,
)

fun RoundedCornerCouponShape(
    topStart: Float = 0.0f,
    topEnd: Float = 0.0f,
    bottomEnd: Float = 0.0f,
    bottomStart: Float = 0.0f,
    middleStartRadius: Float = 0.0f,
    middleEndRadius: Float = 0.0f,
) = RoundedCornerCouponShape(
    topStart = CornerSize(topStart),
    topEnd = CornerSize(topEnd),
    bottomEnd = CornerSize(bottomEnd),
    bottomStart = CornerSize(bottomStart),
    middleStartRadius = middleStartRadius.dp,
    middleEndRadius = middleEndRadius.dp,
)
