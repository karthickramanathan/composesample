package com.sample.compose.effects

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material.icons.rounded.Remove
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

private const val EffectsPageTag = "effects_page_tag"

data class SomeState(val count: Int)

@Composable
fun EffectsPage(
    modifier: Modifier = Modifier,
    contentModifier: Modifier = Modifier,
    onBackClick: () -> Unit
) {

    var count by remember { mutableStateOf(0) }

    Surface (modifier = modifier) {
        Column(modifier = contentModifier) {
            Spacer(modifier = Modifier.height(16.dp))
            Counter(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                count = count,
                increaseCount = { count++ },
                decreaseCount = { count-- },
            )
            Spacer(modifier = Modifier.height(16.dp))
            /*NoChangedParamCheck(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
            )
            Spacer(modifier = Modifier.height(16.dp))
            SideEffectValueCheck(
                count = count,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
            )*/
            Spacer(modifier = Modifier.height(16.dp))
            MutableParamCheck(
                state = SomeState(count = 100),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
            )
            Spacer(modifier = Modifier.height(16.dp))
        }
    }
}

@Composable
private fun Counter(
    count: Int,
    increaseCount: () -> Unit,
    decreaseCount: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Log.d(EffectsPageTag, "Composable : Counter : $count")

    Card(
        modifier = modifier,
        shape = RoundedCornerShape(8.dp),
    ) {
        Row(
            modifier = Modifier
                .height(IntrinsicSize.Min)
                .padding(16.dp),
            horizontalArrangement = Arrangement.SpaceEvenly,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            IconButton(
                modifier = Modifier
                    .background(color = Color.Blue, shape = CircleShape),
                onClick = decreaseCount
            ) {
                Icon(
                    Icons.Rounded.Remove,
                    contentDescription = null,
                    tint = Color.White
                )
            }
            Box(
                modifier = Modifier
                    .fillMaxHeight()
                    .width(200.dp)
                    .border(
                        width = 2.dp,
                        color = Color.Green,
                        shape = RoundedCornerShape(8.dp)
                    ),
                contentAlignment = Alignment.Center,
            ) {
                Text(
                    modifier = Modifier.padding(16.dp),
                    text = count.toString(),
                    fontSize = 20.sp,
                    textAlign = TextAlign.Center,
                    color = Color.Blue,
                )
            }
            IconButton(
                modifier = Modifier
                    .background(color = Color.Blue, shape = CircleShape),
                onClick = increaseCount
            ) {
                Icon(
                    Icons.Rounded.Add,
                    contentDescription = null,
                    tint = Color.White
                )
            }
        }
    }
}

@Composable
private fun NoChangedParamCheck(
    data: Int = 0,
    modifier: Modifier = Modifier
) {

    Log.d(EffectsPageTag, "Composable : NoChangedParamCheck")

    CardText(modifier = modifier, text = "No Changed Param Check")
}

@Composable
private fun SideEffectValueCheck(
    count: Int,
    modifier: Modifier = Modifier
) {

    Log.d(EffectsPageTag, "Composable : SideEffectValueCheck")

    SideEffect {
        Log.d(EffectsPageTag, "Composable : SideEffectValueCheck : $count")
    }

    CardText(modifier = modifier, text = "Side Effect Value Check")
}

@Composable
private fun LaunchedEffectValueCheck(
    count: Int,
    modifier: Modifier = Modifier
) {

    Log.d(EffectsPageTag, "Composable : LaunchedEffectValueCheck")

    LaunchedEffect(count) {
        Log.d(EffectsPageTag, "Composable : LaunchedEffect : $count")
    }

    CardText(modifier = modifier, text = "Launched Effect Value Check")
}

@Composable
private fun DisposeEffectValueCheck(
    count: Int,
    modifier: Modifier = Modifier
) {

    Log.d(EffectsPageTag, "Composable : DisposeEffectValueCheck")

    DisposableEffect(count) {
        Log.d(EffectsPageTag, "Composable : DisposableEffect : $count")

        onDispose {

        }
    }

    CardText(modifier = modifier, text = "Dispose Effect Value Check")
}

@Composable
private fun MutableParamCheck(
    state: SomeState,
    modifier: Modifier = Modifier
) {
    Log.d(EffectsPageTag, "Composable : MutableParamCheck : ${state.count}")

    CardText(modifier = modifier, text = state.count.toString())
}

@Preview
@Composable
private fun EffectsPagePreview() {
    EffectsPage {}
}