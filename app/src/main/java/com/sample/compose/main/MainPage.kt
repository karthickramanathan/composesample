package com.sample.compose.main

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForwardIos
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sample.compose.R
import com.sample.compose.base.utils.convertSnakeCaseToUpperCamelCase
import com.sample.compose.navigation.NavDestinations

@Composable
fun MainPage(
    modifier: Modifier = Modifier,
    navigateTo: (String) -> Unit,
) {

    Scaffold(
        modifier = modifier,
        topBar = {
            TopAppBar(
                title = { Text("Compose Samples") },
                backgroundColor = MaterialTheme.colors.background,
                elevation = 0.dp,
                navigationIcon = {
                    Image(
                        modifier = Modifier.padding(4.dp),
                        painter = painterResource(id = R.drawable.hme),
                        contentDescription = null
                    )
                },
            )
        }
    ) {

        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colors.background)
        ) {
            itemsIndexed(NavDestinations.allDestinations) { index, item ->
                TextButton(
                    modifier = Modifier.padding(8.dp),
                    onClick = { navigateTo(item) }
                ) {
                    Text(
                        modifier = Modifier.weight(1f),
                        text = "${index + 1}. $item",
                        fontSize = 16.sp
                    )
                    Icon(
                        modifier = Modifier.size(20.dp),
                        imageVector = Icons.Default.ArrowForwardIos,
                        contentDescription = null,
                        tint = Color.DarkGray
                    )
                }
            }
        }
    }
}