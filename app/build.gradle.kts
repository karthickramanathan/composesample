plugins {
    id("com.android.application")
    id("kotlin-android")
}

android {
    compileSdk = 30
    buildToolsVersion = "30.0.3"

    defaultConfig {
        applicationId = "com.sample.compose"
        minSdk = 21
        targetSdk = 30
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    buildFeatures {
        compose = true
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
        freeCompilerArgs = freeCompilerArgs + "-Xopt-in=kotlin.RequiresOptIn"
    }
    composeOptions {
        kotlinCompilerExtensionVersion = Libs.Compose.version
    }
}

dependencies {

    implementation(Libs.Kotlin.stdlib)
    implementation(Libs.Kotlin.Coroutines.core)
    implementation(Libs.Kotlin.Coroutines.android)

    implementation(Libs.AndroidX.coreKtx)
    implementation(Libs.AndroidX.Activity.activityCompose)
    implementation(Libs.AndroidX.material)
    implementation(Libs.ConstraintLayout.constraintLayoutCompose)
    implementation(Libs.AndroidX.Lifecycle.viewModelCompose)
    implementation(Libs.Navigation.navigationCompose)

    with(Libs.Compose) {
        implementation(runtime)
        implementation(foundation)
        implementation(layout)
        implementation(ui)
        implementation(uiUtil)
        implementation(animation)
        implementation(material)
        implementation(iconsExtended)
        implementation(tooling)
        debugImplementation(toolingPreview)
    }

    implementation(Libs.ComposeAdditionalLibs.composeCollapsingToolbar)
    implementation(Libs.AdditionalUiLibs.mpChart)

    implementation(Libs.Coil.coilCompose)

    implementation(Libs.Compose.Accompanist.insets)
    implementation(Libs.Compose.Accompanist.systemUiController)
    implementation(Libs.Compose.Accompanist.pager)

    implementation(Libs.Retrofit.retrofit)
    implementation(Libs.Retrofit.retrofitRx)
    implementation(Libs.Retrofit.retrofitGson)

    implementation(Libs.Serialization.gson)

    implementation(Libs.Rx.rxJava)
    implementation(Libs.Rx.rxAndroid)
    implementation(Libs.Rx.rxKotlin)

    debugImplementation(Libs.Compose.Test.testManifest)

    androidTestImplementation(Libs.Test.JUnit.junit)
    androidTestImplementation(Libs.AndroidX.Test.runner)
    androidTestImplementation(Libs.AndroidX.Test.espressoCore)
    androidTestImplementation(Libs.AndroidX.Test.rules)
    androidTestImplementation(Libs.Compose.Test.uiTest)
}